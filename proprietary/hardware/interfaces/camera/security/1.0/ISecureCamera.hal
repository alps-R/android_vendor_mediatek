/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package vendor.mediatek.hardware.camera.security@1.0;

import ISecureCameraClientCallback;
import android.hardware.camera.common@1.0::types;

/**
 * This is tailored for secure camera control.
 */
interface ISecureCamera {
    /**
     * query available secure camera device in string literal.
     *
     * @param cameraDeviceIDs
     * @return cameraDeviceIDs
     *      The vector of available secure camera device IDs.
     */
    getCameraIdList()
            generates (Status status, vec<string> cameraDeviceIDs);

    /**
     *  Query Camera Secure Mode
     *
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         Camera status is not secure mode
     *      CAMERA_IN_USE:
     *         Camera status is secure mode
     * @return secureDeviceInUsedIDs
     *      The vector of in used secure camera device IDs.
     */
    getSecureStatus()
            generates (Status status, vec<string> secureDeviceInUsedIDs);

    /**
     * Open secure camera device(s).
     *
     * @param ids
     *      valid camera id(s) from getCameraIdList(); the order of
     *      opening camera devices is defined in
     *      the HAL server implementation implicitly.
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         On success
     */
    open(vec<string> ids) generates (Status status);

    /**
     * Close secure camera device(s).
     *
     * @param ids
     *      opened camera id(s)
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         On success
     */
    close(vec<string> ids) generates (Status status);

    /**
     * Initialize secure camera device(s) (valid after a successful open()).
     *
     * @param ids
     *      opened camera id(s)
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         On success
     */
    initialize(vec<string> ids) generates (Status status);

    /**
     * Uninitialize secure camera device(s) (valid after a successful open()).
     *
     * @param ids
     *      opened camera id(s)
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         On success
     */
    uninitialize(vec<string> ids) generates (Status status);

    /**
     * Register a callback to receive the capture result(s)
     *
     * @param clientCallback
     *      callback implementation from the HAL client
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         On success
     */
     registerCallback(ISecureCameraClientCallback clientCallback)
         generates (Status status);

    /**
     * Start capture biometric data
     *
     * @param ids
     *      opened camera id(s)
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         On success
     */
     startCapture(vec<string> ids) generates (Status status);

    /**
     * Stop capture biometric data
     *
     * @param ids
     *      opened camera id(s)
     * @return status
     *      Status code for the operation, one of:
     *      OK:
     *         On success
     */
     stopCapture(vec<string> ids) generates (Status status);
};

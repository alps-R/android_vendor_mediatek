/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2019. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

.SLK = {
    s5k3p8spmipiraw_SLK_0000[0],s5k3p8spmipiraw_SLK_0000[1],s5k3p8spmipiraw_SLK_0000[2],s5k3p8spmipiraw_SLK_0000[3],s5k3p8spmipiraw_SLK_0000[4],s5k3p8spmipiraw_SLK_0000[5],s5k3p8spmipiraw_SLK_0000[6],s5k3p8spmipiraw_SLK_0000[7],s5k3p8spmipiraw_SLK_0000[8],s5k3p8spmipiraw_SLK_0000[9],
},
.DM = {
    s5k3p8spmipiraw_DM_0000[0],s5k3p8spmipiraw_DM_0000[1],s5k3p8spmipiraw_DM_0000[2],s5k3p8spmipiraw_DM_0000[3],s5k3p8spmipiraw_DM_0000[4],s5k3p8spmipiraw_DM_0000[5],s5k3p8spmipiraw_DM_0000[6],s5k3p8spmipiraw_DM_0000[7],s5k3p8spmipiraw_DM_0008[0],s5k3p8spmipiraw_DM_0008[1],
    s5k3p8spmipiraw_DM_0008[2],s5k3p8spmipiraw_DM_0008[3],s5k3p8spmipiraw_DM_0008[4],s5k3p8spmipiraw_DM_0008[5],s5k3p8spmipiraw_DM_0008[6],s5k3p8spmipiraw_DM_0008[7],s5k3p8spmipiraw_DM_0016[0],s5k3p8spmipiraw_DM_0016[1],s5k3p8spmipiraw_DM_0016[2],s5k3p8spmipiraw_DM_0016[3],
    s5k3p8spmipiraw_DM_0016[4],s5k3p8spmipiraw_DM_0016[5],s5k3p8spmipiraw_DM_0016[6],s5k3p8spmipiraw_DM_0016[7],s5k3p8spmipiraw_DM_0016[8],s5k3p8spmipiraw_DM_0016[9],s5k3p8spmipiraw_DM_0016[10],s5k3p8spmipiraw_DM_0016[11],s5k3p8spmipiraw_DM_0016[12],s5k3p8spmipiraw_DM_0016[13],
    s5k3p8spmipiraw_DM_0016[14],s5k3p8spmipiraw_DM_0016[15],s5k3p8spmipiraw_DM_0032[0],s5k3p8spmipiraw_DM_0032[1],s5k3p8spmipiraw_DM_0032[2],s5k3p8spmipiraw_DM_0032[3],s5k3p8spmipiraw_DM_0032[4],s5k3p8spmipiraw_DM_0032[5],s5k3p8spmipiraw_DM_0032[6],s5k3p8spmipiraw_DM_0032[7],
    s5k3p8spmipiraw_DM_0040[0],s5k3p8spmipiraw_DM_0040[1],s5k3p8spmipiraw_DM_0040[2],s5k3p8spmipiraw_DM_0040[3],s5k3p8spmipiraw_DM_0040[4],s5k3p8spmipiraw_DM_0040[5],s5k3p8spmipiraw_DM_0040[6],s5k3p8spmipiraw_DM_0040[7],s5k3p8spmipiraw_DM_0048[0],s5k3p8spmipiraw_DM_0048[1],
    s5k3p8spmipiraw_DM_0048[2],s5k3p8spmipiraw_DM_0048[3],s5k3p8spmipiraw_DM_0048[4],s5k3p8spmipiraw_DM_0048[5],s5k3p8spmipiraw_DM_0048[6],s5k3p8spmipiraw_DM_0048[7],s5k3p8spmipiraw_DM_0056[0],s5k3p8spmipiraw_DM_0056[1],s5k3p8spmipiraw_DM_0056[2],s5k3p8spmipiraw_DM_0056[3],
    s5k3p8spmipiraw_DM_0056[4],s5k3p8spmipiraw_DM_0056[5],s5k3p8spmipiraw_DM_0056[6],s5k3p8spmipiraw_DM_0056[7],s5k3p8spmipiraw_DM_0064[0],s5k3p8spmipiraw_DM_0064[1],s5k3p8spmipiraw_DM_0064[2],s5k3p8spmipiraw_DM_0064[3],s5k3p8spmipiraw_DM_0064[4],s5k3p8spmipiraw_DM_0064[5],
    s5k3p8spmipiraw_DM_0064[6],s5k3p8spmipiraw_DM_0064[7],
},
.YNR = {
    s5k3p8spmipiraw_YNR_0000[0],s5k3p8spmipiraw_YNR_0000[1],s5k3p8spmipiraw_YNR_0000[2],s5k3p8spmipiraw_YNR_0000[3],s5k3p8spmipiraw_YNR_0000[4],s5k3p8spmipiraw_YNR_0000[5],s5k3p8spmipiraw_YNR_0000[6],s5k3p8spmipiraw_YNR_0000[7],s5k3p8spmipiraw_YNR_0008[0],s5k3p8spmipiraw_YNR_0008[1],
    s5k3p8spmipiraw_YNR_0008[2],s5k3p8spmipiraw_YNR_0008[3],s5k3p8spmipiraw_YNR_0008[4],s5k3p8spmipiraw_YNR_0008[5],s5k3p8spmipiraw_YNR_0008[6],s5k3p8spmipiraw_YNR_0008[7],s5k3p8spmipiraw_YNR_0016[0],s5k3p8spmipiraw_YNR_0016[1],s5k3p8spmipiraw_YNR_0016[2],s5k3p8spmipiraw_YNR_0016[3],
    s5k3p8spmipiraw_YNR_0016[4],s5k3p8spmipiraw_YNR_0016[5],s5k3p8spmipiraw_YNR_0016[6],s5k3p8spmipiraw_YNR_0016[7],s5k3p8spmipiraw_YNR_0016[8],s5k3p8spmipiraw_YNR_0016[9],s5k3p8spmipiraw_YNR_0016[10],s5k3p8spmipiraw_YNR_0016[11],s5k3p8spmipiraw_YNR_0016[12],s5k3p8spmipiraw_YNR_0016[13],
    s5k3p8spmipiraw_YNR_0016[14],s5k3p8spmipiraw_YNR_0016[15],s5k3p8spmipiraw_YNR_0016[16],s5k3p8spmipiraw_YNR_0016[17],s5k3p8spmipiraw_YNR_0016[18],s5k3p8spmipiraw_YNR_0016[19],s5k3p8spmipiraw_YNR_0016[20],s5k3p8spmipiraw_YNR_0016[21],s5k3p8spmipiraw_YNR_0016[22],s5k3p8spmipiraw_YNR_0016[23],
    s5k3p8spmipiraw_YNR_0040[0],s5k3p8spmipiraw_YNR_0040[1],s5k3p8spmipiraw_YNR_0040[2],s5k3p8spmipiraw_YNR_0040[3],s5k3p8spmipiraw_YNR_0040[4],s5k3p8spmipiraw_YNR_0040[5],s5k3p8spmipiraw_YNR_0040[6],s5k3p8spmipiraw_YNR_0040[7],s5k3p8spmipiraw_YNR_0040[8],s5k3p8spmipiraw_YNR_0040[9],
    s5k3p8spmipiraw_YNR_0040[10],s5k3p8spmipiraw_YNR_0040[11],s5k3p8spmipiraw_YNR_0040[12],s5k3p8spmipiraw_YNR_0040[13],s5k3p8spmipiraw_YNR_0040[14],s5k3p8spmipiraw_YNR_0040[15],s5k3p8spmipiraw_YNR_0056[0],s5k3p8spmipiraw_YNR_0056[1],s5k3p8spmipiraw_YNR_0056[2],s5k3p8spmipiraw_YNR_0056[3],
    s5k3p8spmipiraw_YNR_0056[4],s5k3p8spmipiraw_YNR_0056[5],s5k3p8spmipiraw_YNR_0056[6],s5k3p8spmipiraw_YNR_0056[7],s5k3p8spmipiraw_YNR_0064[0],s5k3p8spmipiraw_YNR_0064[1],s5k3p8spmipiraw_YNR_0064[2],s5k3p8spmipiraw_YNR_0064[3],s5k3p8spmipiraw_YNR_0064[4],s5k3p8spmipiraw_YNR_0064[5],
    s5k3p8spmipiraw_YNR_0064[6],s5k3p8spmipiraw_YNR_0064[7],s5k3p8spmipiraw_YNR_0072[0],s5k3p8spmipiraw_YNR_0072[1],s5k3p8spmipiraw_YNR_0072[2],s5k3p8spmipiraw_YNR_0072[3],s5k3p8spmipiraw_YNR_0072[4],s5k3p8spmipiraw_YNR_0072[5],s5k3p8spmipiraw_YNR_0072[6],s5k3p8spmipiraw_YNR_0072[7],
    s5k3p8spmipiraw_YNR_0072[8],s5k3p8spmipiraw_YNR_0072[9],s5k3p8spmipiraw_YNR_0072[10],s5k3p8spmipiraw_YNR_0072[11],s5k3p8spmipiraw_YNR_0072[12],s5k3p8spmipiraw_YNR_0072[13],s5k3p8spmipiraw_YNR_0072[14],s5k3p8spmipiraw_YNR_0072[15],s5k3p8spmipiraw_YNR_0072[16],s5k3p8spmipiraw_YNR_0072[17],
    s5k3p8spmipiraw_YNR_0072[18],s5k3p8spmipiraw_YNR_0072[19],s5k3p8spmipiraw_YNR_0072[20],s5k3p8spmipiraw_YNR_0072[21],s5k3p8spmipiraw_YNR_0072[22],s5k3p8spmipiraw_YNR_0072[23],s5k3p8spmipiraw_YNR_0096[0],s5k3p8spmipiraw_YNR_0096[1],s5k3p8spmipiraw_YNR_0096[2],s5k3p8spmipiraw_YNR_0096[3],
    s5k3p8spmipiraw_YNR_0096[4],s5k3p8spmipiraw_YNR_0096[5],s5k3p8spmipiraw_YNR_0096[6],s5k3p8spmipiraw_YNR_0096[7],s5k3p8spmipiraw_YNR_0104[0],s5k3p8spmipiraw_YNR_0104[1],s5k3p8spmipiraw_YNR_0104[2],s5k3p8spmipiraw_YNR_0104[3],s5k3p8spmipiraw_YNR_0104[4],s5k3p8spmipiraw_YNR_0104[5],
    s5k3p8spmipiraw_YNR_0104[6],s5k3p8spmipiraw_YNR_0104[7],
},
.YNR_LCE_LINK = {
    s5k3p8spmipiraw_YNR_LCE_LINK_0000[0],s5k3p8spmipiraw_YNR_LCE_LINK_0000[1],s5k3p8spmipiraw_YNR_LCE_LINK_0000[2],s5k3p8spmipiraw_YNR_LCE_LINK_0000[3],s5k3p8spmipiraw_YNR_LCE_LINK_0000[4],s5k3p8spmipiraw_YNR_LCE_LINK_0000[5],s5k3p8spmipiraw_YNR_LCE_LINK_0000[6],s5k3p8spmipiraw_YNR_LCE_LINK_0000[7],s5k3p8spmipiraw_YNR_LCE_LINK_0008[0],s5k3p8spmipiraw_YNR_LCE_LINK_0008[1],
    s5k3p8spmipiraw_YNR_LCE_LINK_0008[2],s5k3p8spmipiraw_YNR_LCE_LINK_0008[3],s5k3p8spmipiraw_YNR_LCE_LINK_0008[4],s5k3p8spmipiraw_YNR_LCE_LINK_0008[5],s5k3p8spmipiraw_YNR_LCE_LINK_0008[6],s5k3p8spmipiraw_YNR_LCE_LINK_0008[7],s5k3p8spmipiraw_YNR_LCE_LINK_0008[8],s5k3p8spmipiraw_YNR_LCE_LINK_0008[9],s5k3p8spmipiraw_YNR_LCE_LINK_0008[10],s5k3p8spmipiraw_YNR_LCE_LINK_0008[11],
    s5k3p8spmipiraw_YNR_LCE_LINK_0008[12],s5k3p8spmipiraw_YNR_LCE_LINK_0008[13],s5k3p8spmipiraw_YNR_LCE_LINK_0008[14],s5k3p8spmipiraw_YNR_LCE_LINK_0008[15],s5k3p8spmipiraw_YNR_LCE_LINK_0024[0],s5k3p8spmipiraw_YNR_LCE_LINK_0024[1],s5k3p8spmipiraw_YNR_LCE_LINK_0024[2],s5k3p8spmipiraw_YNR_LCE_LINK_0024[3],s5k3p8spmipiraw_YNR_LCE_LINK_0024[4],s5k3p8spmipiraw_YNR_LCE_LINK_0024[5],
    s5k3p8spmipiraw_YNR_LCE_LINK_0024[6],s5k3p8spmipiraw_YNR_LCE_LINK_0024[7],s5k3p8spmipiraw_YNR_LCE_LINK_0032[0],s5k3p8spmipiraw_YNR_LCE_LINK_0032[1],s5k3p8spmipiraw_YNR_LCE_LINK_0032[2],s5k3p8spmipiraw_YNR_LCE_LINK_0032[3],s5k3p8spmipiraw_YNR_LCE_LINK_0032[4],s5k3p8spmipiraw_YNR_LCE_LINK_0032[5],s5k3p8spmipiraw_YNR_LCE_LINK_0032[6],s5k3p8spmipiraw_YNR_LCE_LINK_0032[7],
    s5k3p8spmipiraw_YNR_LCE_LINK_0040[0],s5k3p8spmipiraw_YNR_LCE_LINK_0040[1],s5k3p8spmipiraw_YNR_LCE_LINK_0040[2],s5k3p8spmipiraw_YNR_LCE_LINK_0040[3],s5k3p8spmipiraw_YNR_LCE_LINK_0040[4],s5k3p8spmipiraw_YNR_LCE_LINK_0040[5],s5k3p8spmipiraw_YNR_LCE_LINK_0040[6],s5k3p8spmipiraw_YNR_LCE_LINK_0040[7],
},
.YNR_FD = {
    s5k3p8spmipiraw_YNR_FD_0000[0],s5k3p8spmipiraw_YNR_FD_0000[1],s5k3p8spmipiraw_YNR_FD_0000[2],s5k3p8spmipiraw_YNR_FD_0000[3],s5k3p8spmipiraw_YNR_FD_0000[4],s5k3p8spmipiraw_YNR_FD_0000[5],s5k3p8spmipiraw_YNR_FD_0000[6],s5k3p8spmipiraw_YNR_FD_0000[7],s5k3p8spmipiraw_YNR_FD_0008[0],s5k3p8spmipiraw_YNR_FD_0008[1],
    s5k3p8spmipiraw_YNR_FD_0008[2],s5k3p8spmipiraw_YNR_FD_0008[3],s5k3p8spmipiraw_YNR_FD_0008[4],s5k3p8spmipiraw_YNR_FD_0008[5],s5k3p8spmipiraw_YNR_FD_0008[6],s5k3p8spmipiraw_YNR_FD_0008[7],s5k3p8spmipiraw_YNR_FD_0008[8],s5k3p8spmipiraw_YNR_FD_0008[9],s5k3p8spmipiraw_YNR_FD_0008[10],s5k3p8spmipiraw_YNR_FD_0008[11],
    s5k3p8spmipiraw_YNR_FD_0008[12],s5k3p8spmipiraw_YNR_FD_0008[13],s5k3p8spmipiraw_YNR_FD_0008[14],s5k3p8spmipiraw_YNR_FD_0008[15],s5k3p8spmipiraw_YNR_FD_0024[0],s5k3p8spmipiraw_YNR_FD_0024[1],s5k3p8spmipiraw_YNR_FD_0024[2],s5k3p8spmipiraw_YNR_FD_0024[3],s5k3p8spmipiraw_YNR_FD_0024[4],s5k3p8spmipiraw_YNR_FD_0024[5],
    s5k3p8spmipiraw_YNR_FD_0024[6],s5k3p8spmipiraw_YNR_FD_0024[7],s5k3p8spmipiraw_YNR_FD_0032[0],s5k3p8spmipiraw_YNR_FD_0032[1],s5k3p8spmipiraw_YNR_FD_0032[2],s5k3p8spmipiraw_YNR_FD_0032[3],s5k3p8spmipiraw_YNR_FD_0032[4],s5k3p8spmipiraw_YNR_FD_0032[5],s5k3p8spmipiraw_YNR_FD_0032[6],s5k3p8spmipiraw_YNR_FD_0032[7],
    s5k3p8spmipiraw_YNR_FD_0040[0],s5k3p8spmipiraw_YNR_FD_0040[1],s5k3p8spmipiraw_YNR_FD_0040[2],s5k3p8spmipiraw_YNR_FD_0040[3],s5k3p8spmipiraw_YNR_FD_0040[4],s5k3p8spmipiraw_YNR_FD_0040[5],s5k3p8spmipiraw_YNR_FD_0040[6],s5k3p8spmipiraw_YNR_FD_0040[7],
},
.CNR_CNR = {
    s5k3p8spmipiraw_CNR_CNR_0000[0],s5k3p8spmipiraw_CNR_CNR_0000[1],s5k3p8spmipiraw_CNR_CNR_0000[2],s5k3p8spmipiraw_CNR_CNR_0000[3],s5k3p8spmipiraw_CNR_CNR_0000[4],s5k3p8spmipiraw_CNR_CNR_0000[5],s5k3p8spmipiraw_CNR_CNR_0000[6],s5k3p8spmipiraw_CNR_CNR_0000[7],s5k3p8spmipiraw_CNR_CNR_0008[0],s5k3p8spmipiraw_CNR_CNR_0008[1],
    s5k3p8spmipiraw_CNR_CNR_0008[2],s5k3p8spmipiraw_CNR_CNR_0008[3],s5k3p8spmipiraw_CNR_CNR_0008[4],s5k3p8spmipiraw_CNR_CNR_0008[5],s5k3p8spmipiraw_CNR_CNR_0008[6],s5k3p8spmipiraw_CNR_CNR_0008[7],s5k3p8spmipiraw_CNR_CNR_0008[8],s5k3p8spmipiraw_CNR_CNR_0008[9],s5k3p8spmipiraw_CNR_CNR_0008[10],s5k3p8spmipiraw_CNR_CNR_0008[11],
    s5k3p8spmipiraw_CNR_CNR_0008[12],s5k3p8spmipiraw_CNR_CNR_0008[13],s5k3p8spmipiraw_CNR_CNR_0008[14],s5k3p8spmipiraw_CNR_CNR_0008[15],s5k3p8spmipiraw_CNR_CNR_0008[16],s5k3p8spmipiraw_CNR_CNR_0008[17],s5k3p8spmipiraw_CNR_CNR_0008[18],s5k3p8spmipiraw_CNR_CNR_0008[19],s5k3p8spmipiraw_CNR_CNR_0008[20],s5k3p8spmipiraw_CNR_CNR_0008[21],
    s5k3p8spmipiraw_CNR_CNR_0008[22],s5k3p8spmipiraw_CNR_CNR_0008[23],s5k3p8spmipiraw_CNR_CNR_0032[0],s5k3p8spmipiraw_CNR_CNR_0032[1],s5k3p8spmipiraw_CNR_CNR_0032[2],s5k3p8spmipiraw_CNR_CNR_0032[3],s5k3p8spmipiraw_CNR_CNR_0032[4],s5k3p8spmipiraw_CNR_CNR_0032[5],s5k3p8spmipiraw_CNR_CNR_0032[6],s5k3p8spmipiraw_CNR_CNR_0032[7],
    s5k3p8spmipiraw_CNR_CNR_0032[8],s5k3p8spmipiraw_CNR_CNR_0032[9],s5k3p8spmipiraw_CNR_CNR_0032[10],s5k3p8spmipiraw_CNR_CNR_0032[11],s5k3p8spmipiraw_CNR_CNR_0032[12],s5k3p8spmipiraw_CNR_CNR_0032[13],s5k3p8spmipiraw_CNR_CNR_0032[14],s5k3p8spmipiraw_CNR_CNR_0032[15],s5k3p8spmipiraw_CNR_CNR_0048[0],s5k3p8spmipiraw_CNR_CNR_0048[1],
    s5k3p8spmipiraw_CNR_CNR_0048[2],s5k3p8spmipiraw_CNR_CNR_0048[3],s5k3p8spmipiraw_CNR_CNR_0048[4],s5k3p8spmipiraw_CNR_CNR_0048[5],s5k3p8spmipiraw_CNR_CNR_0048[6],s5k3p8spmipiraw_CNR_CNR_0048[7],s5k3p8spmipiraw_CNR_CNR_0056[0],s5k3p8spmipiraw_CNR_CNR_0056[1],s5k3p8spmipiraw_CNR_CNR_0056[2],s5k3p8spmipiraw_CNR_CNR_0056[3],
    s5k3p8spmipiraw_CNR_CNR_0056[4],s5k3p8spmipiraw_CNR_CNR_0056[5],s5k3p8spmipiraw_CNR_CNR_0056[6],s5k3p8spmipiraw_CNR_CNR_0056[7],s5k3p8spmipiraw_CNR_CNR_0056[8],s5k3p8spmipiraw_CNR_CNR_0056[9],s5k3p8spmipiraw_CNR_CNR_0056[10],s5k3p8spmipiraw_CNR_CNR_0056[11],s5k3p8spmipiraw_CNR_CNR_0056[12],s5k3p8spmipiraw_CNR_CNR_0056[13],
    s5k3p8spmipiraw_CNR_CNR_0056[14],s5k3p8spmipiraw_CNR_CNR_0056[15],s5k3p8spmipiraw_CNR_CNR_0056[16],s5k3p8spmipiraw_CNR_CNR_0056[17],s5k3p8spmipiraw_CNR_CNR_0056[18],s5k3p8spmipiraw_CNR_CNR_0056[19],s5k3p8spmipiraw_CNR_CNR_0056[20],s5k3p8spmipiraw_CNR_CNR_0056[21],s5k3p8spmipiraw_CNR_CNR_0056[22],s5k3p8spmipiraw_CNR_CNR_0056[23],
    s5k3p8spmipiraw_CNR_CNR_0080[0],s5k3p8spmipiraw_CNR_CNR_0080[1],s5k3p8spmipiraw_CNR_CNR_0080[2],s5k3p8spmipiraw_CNR_CNR_0080[3],s5k3p8spmipiraw_CNR_CNR_0080[4],s5k3p8spmipiraw_CNR_CNR_0080[5],s5k3p8spmipiraw_CNR_CNR_0080[6],s5k3p8spmipiraw_CNR_CNR_0080[7],s5k3p8spmipiraw_CNR_CNR_0088[0],s5k3p8spmipiraw_CNR_CNR_0088[1],
    s5k3p8spmipiraw_CNR_CNR_0088[2],s5k3p8spmipiraw_CNR_CNR_0088[3],s5k3p8spmipiraw_CNR_CNR_0088[4],s5k3p8spmipiraw_CNR_CNR_0088[5],s5k3p8spmipiraw_CNR_CNR_0088[6],s5k3p8spmipiraw_CNR_CNR_0088[7],
},
.EE = {
    s5k3p8spmipiraw_EE_0000[0],s5k3p8spmipiraw_EE_0000[1],s5k3p8spmipiraw_EE_0000[2],s5k3p8spmipiraw_EE_0000[3],s5k3p8spmipiraw_EE_0000[4],s5k3p8spmipiraw_EE_0000[5],s5k3p8spmipiraw_EE_0000[6],s5k3p8spmipiraw_EE_0000[7],s5k3p8spmipiraw_EE_0008[0],s5k3p8spmipiraw_EE_0008[1],
    s5k3p8spmipiraw_EE_0008[2],s5k3p8spmipiraw_EE_0008[3],s5k3p8spmipiraw_EE_0008[4],s5k3p8spmipiraw_EE_0008[5],s5k3p8spmipiraw_EE_0008[6],s5k3p8spmipiraw_EE_0008[7],s5k3p8spmipiraw_EE_0016[0],s5k3p8spmipiraw_EE_0016[1],s5k3p8spmipiraw_EE_0016[2],s5k3p8spmipiraw_EE_0016[3],
    s5k3p8spmipiraw_EE_0016[4],s5k3p8spmipiraw_EE_0016[5],s5k3p8spmipiraw_EE_0016[6],s5k3p8spmipiraw_EE_0016[7],s5k3p8spmipiraw_EE_0024[0],s5k3p8spmipiraw_EE_0024[1],s5k3p8spmipiraw_EE_0024[2],s5k3p8spmipiraw_EE_0024[3],s5k3p8spmipiraw_EE_0024[4],s5k3p8spmipiraw_EE_0024[5],
    s5k3p8spmipiraw_EE_0024[6],s5k3p8spmipiraw_EE_0024[7],s5k3p8spmipiraw_EE_0024[8],s5k3p8spmipiraw_EE_0024[9],s5k3p8spmipiraw_EE_0024[10],s5k3p8spmipiraw_EE_0024[11],s5k3p8spmipiraw_EE_0024[12],s5k3p8spmipiraw_EE_0024[13],s5k3p8spmipiraw_EE_0024[14],s5k3p8spmipiraw_EE_0024[15],
    s5k3p8spmipiraw_EE_0040[0],s5k3p8spmipiraw_EE_0040[1],s5k3p8spmipiraw_EE_0040[2],s5k3p8spmipiraw_EE_0040[3],s5k3p8spmipiraw_EE_0040[4],s5k3p8spmipiraw_EE_0040[5],s5k3p8spmipiraw_EE_0040[6],s5k3p8spmipiraw_EE_0040[7],s5k3p8spmipiraw_EE_0048[0],s5k3p8spmipiraw_EE_0048[1],
    s5k3p8spmipiraw_EE_0048[2],s5k3p8spmipiraw_EE_0048[3],s5k3p8spmipiraw_EE_0048[4],s5k3p8spmipiraw_EE_0048[5],s5k3p8spmipiraw_EE_0048[6],s5k3p8spmipiraw_EE_0048[7],s5k3p8spmipiraw_EE_0056[0],s5k3p8spmipiraw_EE_0056[1],s5k3p8spmipiraw_EE_0056[2],s5k3p8spmipiraw_EE_0056[3],
    s5k3p8spmipiraw_EE_0056[4],s5k3p8spmipiraw_EE_0056[5],s5k3p8spmipiraw_EE_0056[6],s5k3p8spmipiraw_EE_0056[7],s5k3p8spmipiraw_EE_0064[0],s5k3p8spmipiraw_EE_0064[1],s5k3p8spmipiraw_EE_0064[2],s5k3p8spmipiraw_EE_0064[3],s5k3p8spmipiraw_EE_0064[4],s5k3p8spmipiraw_EE_0064[5],
    s5k3p8spmipiraw_EE_0064[6],s5k3p8spmipiraw_EE_0064[7],s5k3p8spmipiraw_EE_0072[0],s5k3p8spmipiraw_EE_0072[1],s5k3p8spmipiraw_EE_0072[2],s5k3p8spmipiraw_EE_0072[3],s5k3p8spmipiraw_EE_0072[4],s5k3p8spmipiraw_EE_0072[5],s5k3p8spmipiraw_EE_0072[6],s5k3p8spmipiraw_EE_0072[7],
},
.NR3D = {
    s5k3p8spmipiraw_NR3D_0000[0],s5k3p8spmipiraw_NR3D_0000[1],s5k3p8spmipiraw_NR3D_0000[2],s5k3p8spmipiraw_NR3D_0000[3],s5k3p8spmipiraw_NR3D_0000[4],s5k3p8spmipiraw_NR3D_0000[5],s5k3p8spmipiraw_NR3D_0000[6],s5k3p8spmipiraw_NR3D_0000[7],s5k3p8spmipiraw_NR3D_0008[0],s5k3p8spmipiraw_NR3D_0008[1],
    s5k3p8spmipiraw_NR3D_0008[2],s5k3p8spmipiraw_NR3D_0008[3],s5k3p8spmipiraw_NR3D_0008[4],s5k3p8spmipiraw_NR3D_0008[5],s5k3p8spmipiraw_NR3D_0008[6],s5k3p8spmipiraw_NR3D_0008[7],s5k3p8spmipiraw_NR3D_0016[0],s5k3p8spmipiraw_NR3D_0016[1],s5k3p8spmipiraw_NR3D_0016[2],s5k3p8spmipiraw_NR3D_0016[3],
    s5k3p8spmipiraw_NR3D_0016[4],s5k3p8spmipiraw_NR3D_0016[5],s5k3p8spmipiraw_NR3D_0016[6],s5k3p8spmipiraw_NR3D_0016[7],s5k3p8spmipiraw_NR3D_0024[0],s5k3p8spmipiraw_NR3D_0024[1],s5k3p8spmipiraw_NR3D_0024[2],s5k3p8spmipiraw_NR3D_0024[3],s5k3p8spmipiraw_NR3D_0024[4],s5k3p8spmipiraw_NR3D_0024[5],
    s5k3p8spmipiraw_NR3D_0024[6],s5k3p8spmipiraw_NR3D_0024[7],
},
.OBC = {
    s5k3p8spmipiraw_OBC_0000[0],s5k3p8spmipiraw_OBC_0000[1],s5k3p8spmipiraw_OBC_0000[2],s5k3p8spmipiraw_OBC_0000[3],s5k3p8spmipiraw_OBC_0000[4],s5k3p8spmipiraw_OBC_0000[5],s5k3p8spmipiraw_OBC_0000[6],s5k3p8spmipiraw_OBC_0000[7],s5k3p8spmipiraw_OBC_0008[0],s5k3p8spmipiraw_OBC_0008[1],
    s5k3p8spmipiraw_OBC_0008[2],s5k3p8spmipiraw_OBC_0008[3],s5k3p8spmipiraw_OBC_0008[4],s5k3p8spmipiraw_OBC_0008[5],s5k3p8spmipiraw_OBC_0008[6],s5k3p8spmipiraw_OBC_0008[7],s5k3p8spmipiraw_OBC_0016[0],s5k3p8spmipiraw_OBC_0016[1],s5k3p8spmipiraw_OBC_0016[2],s5k3p8spmipiraw_OBC_0016[3],
    s5k3p8spmipiraw_OBC_0016[4],s5k3p8spmipiraw_OBC_0016[5],s5k3p8spmipiraw_OBC_0016[6],s5k3p8spmipiraw_OBC_0016[7],
},
.BPC_BPC = {
    s5k3p8spmipiraw_BPC_BPC_0000[0],s5k3p8spmipiraw_BPC_BPC_0000[1],s5k3p8spmipiraw_BPC_BPC_0000[2],s5k3p8spmipiraw_BPC_BPC_0000[3],s5k3p8spmipiraw_BPC_BPC_0000[4],s5k3p8spmipiraw_BPC_BPC_0000[5],s5k3p8spmipiraw_BPC_BPC_0000[6],s5k3p8spmipiraw_BPC_BPC_0000[7],s5k3p8spmipiraw_BPC_BPC_0008[0],s5k3p8spmipiraw_BPC_BPC_0008[1],
    s5k3p8spmipiraw_BPC_BPC_0008[2],s5k3p8spmipiraw_BPC_BPC_0008[3],s5k3p8spmipiraw_BPC_BPC_0008[4],s5k3p8spmipiraw_BPC_BPC_0008[5],s5k3p8spmipiraw_BPC_BPC_0008[6],s5k3p8spmipiraw_BPC_BPC_0008[7],s5k3p8spmipiraw_BPC_BPC_0016[0],s5k3p8spmipiraw_BPC_BPC_0016[1],s5k3p8spmipiraw_BPC_BPC_0016[2],s5k3p8spmipiraw_BPC_BPC_0016[3],
    s5k3p8spmipiraw_BPC_BPC_0016[4],s5k3p8spmipiraw_BPC_BPC_0016[5],s5k3p8spmipiraw_BPC_BPC_0016[6],s5k3p8spmipiraw_BPC_BPC_0016[7],
},
.BPC_CT = {
    s5k3p8spmipiraw_BPC_CT_0000[0],s5k3p8spmipiraw_BPC_CT_0000[1],s5k3p8spmipiraw_BPC_CT_0000[2],s5k3p8spmipiraw_BPC_CT_0000[3],s5k3p8spmipiraw_BPC_CT_0000[4],s5k3p8spmipiraw_BPC_CT_0000[5],s5k3p8spmipiraw_BPC_CT_0000[6],s5k3p8spmipiraw_BPC_CT_0000[7],s5k3p8spmipiraw_BPC_CT_0008[0],s5k3p8spmipiraw_BPC_CT_0008[1],
    s5k3p8spmipiraw_BPC_CT_0008[2],s5k3p8spmipiraw_BPC_CT_0008[3],s5k3p8spmipiraw_BPC_CT_0008[4],s5k3p8spmipiraw_BPC_CT_0008[5],s5k3p8spmipiraw_BPC_CT_0008[6],s5k3p8spmipiraw_BPC_CT_0008[7],s5k3p8spmipiraw_BPC_CT_0016[0],s5k3p8spmipiraw_BPC_CT_0016[1],s5k3p8spmipiraw_BPC_CT_0016[2],s5k3p8spmipiraw_BPC_CT_0016[3],
    s5k3p8spmipiraw_BPC_CT_0016[4],s5k3p8spmipiraw_BPC_CT_0016[5],s5k3p8spmipiraw_BPC_CT_0016[6],s5k3p8spmipiraw_BPC_CT_0016[7],
},
.BPC_PDC = {
    s5k3p8spmipiraw_BPC_PDC_0000[0],s5k3p8spmipiraw_BPC_PDC_0000[1],s5k3p8spmipiraw_BPC_PDC_0000[2],s5k3p8spmipiraw_BPC_PDC_0000[3],s5k3p8spmipiraw_BPC_PDC_0000[4],s5k3p8spmipiraw_BPC_PDC_0000[5],s5k3p8spmipiraw_BPC_PDC_0000[6],s5k3p8spmipiraw_BPC_PDC_0000[7],s5k3p8spmipiraw_BPC_PDC_0008[0],s5k3p8spmipiraw_BPC_PDC_0008[1],
    s5k3p8spmipiraw_BPC_PDC_0008[2],s5k3p8spmipiraw_BPC_PDC_0008[3],s5k3p8spmipiraw_BPC_PDC_0008[4],s5k3p8spmipiraw_BPC_PDC_0008[5],s5k3p8spmipiraw_BPC_PDC_0008[6],s5k3p8spmipiraw_BPC_PDC_0008[7],s5k3p8spmipiraw_BPC_PDC_0016[0],s5k3p8spmipiraw_BPC_PDC_0016[1],s5k3p8spmipiraw_BPC_PDC_0016[2],s5k3p8spmipiraw_BPC_PDC_0016[3],
    s5k3p8spmipiraw_BPC_PDC_0016[4],s5k3p8spmipiraw_BPC_PDC_0016[5],s5k3p8spmipiraw_BPC_PDC_0016[6],s5k3p8spmipiraw_BPC_PDC_0016[7],
},
.LDNR = {
    s5k3p8spmipiraw_LDNR_0000[0],s5k3p8spmipiraw_LDNR_0000[1],s5k3p8spmipiraw_LDNR_0000[2],s5k3p8spmipiraw_LDNR_0000[3],s5k3p8spmipiraw_LDNR_0000[4],s5k3p8spmipiraw_LDNR_0000[5],s5k3p8spmipiraw_LDNR_0000[6],s5k3p8spmipiraw_LDNR_0000[7],s5k3p8spmipiraw_LDNR_0008[0],s5k3p8spmipiraw_LDNR_0008[1],
    s5k3p8spmipiraw_LDNR_0008[2],s5k3p8spmipiraw_LDNR_0008[3],s5k3p8spmipiraw_LDNR_0008[4],s5k3p8spmipiraw_LDNR_0008[5],s5k3p8spmipiraw_LDNR_0008[6],s5k3p8spmipiraw_LDNR_0008[7],s5k3p8spmipiraw_LDNR_0016[0],s5k3p8spmipiraw_LDNR_0016[1],s5k3p8spmipiraw_LDNR_0016[2],s5k3p8spmipiraw_LDNR_0016[3],
    s5k3p8spmipiraw_LDNR_0016[4],s5k3p8spmipiraw_LDNR_0016[5],s5k3p8spmipiraw_LDNR_0016[6],s5k3p8spmipiraw_LDNR_0016[7],
},
.CNR_CCR = {
    s5k3p8spmipiraw_CNR_CCR_0000[0],s5k3p8spmipiraw_CNR_CCR_0000[1],s5k3p8spmipiraw_CNR_CCR_0000[2],s5k3p8spmipiraw_CNR_CCR_0000[3],s5k3p8spmipiraw_CNR_CCR_0000[4],s5k3p8spmipiraw_CNR_CCR_0000[5],s5k3p8spmipiraw_CNR_CCR_0000[6],s5k3p8spmipiraw_CNR_CCR_0000[7],
},
.CNR_ABF = {
    s5k3p8spmipiraw_CNR_ABF_0000[0],s5k3p8spmipiraw_CNR_ABF_0000[1],s5k3p8spmipiraw_CNR_ABF_0000[2],s5k3p8spmipiraw_CNR_ABF_0000[3],s5k3p8spmipiraw_CNR_ABF_0000[4],s5k3p8spmipiraw_CNR_ABF_0000[5],s5k3p8spmipiraw_CNR_ABF_0000[6],s5k3p8spmipiraw_CNR_ABF_0000[7],
},
.LPCNR = {
    s5k3p8spmipiraw_LPCNR_0000[0],s5k3p8spmipiraw_LPCNR_0000[1],s5k3p8spmipiraw_LPCNR_0000[2],s5k3p8spmipiraw_LPCNR_0000[3],s5k3p8spmipiraw_LPCNR_0000[4],s5k3p8spmipiraw_LPCNR_0000[5],s5k3p8spmipiraw_LPCNR_0000[6],s5k3p8spmipiraw_LPCNR_0000[7],s5k3p8spmipiraw_LPCNR_0008[0],s5k3p8spmipiraw_LPCNR_0008[1],
    s5k3p8spmipiraw_LPCNR_0008[2],s5k3p8spmipiraw_LPCNR_0008[3],s5k3p8spmipiraw_LPCNR_0008[4],s5k3p8spmipiraw_LPCNR_0008[5],s5k3p8spmipiraw_LPCNR_0008[6],s5k3p8spmipiraw_LPCNR_0008[7],s5k3p8spmipiraw_LPCNR_0016[0],s5k3p8spmipiraw_LPCNR_0016[1],s5k3p8spmipiraw_LPCNR_0016[2],s5k3p8spmipiraw_LPCNR_0016[3],
    s5k3p8spmipiraw_LPCNR_0016[4],s5k3p8spmipiraw_LPCNR_0016[5],s5k3p8spmipiraw_LPCNR_0016[6],s5k3p8spmipiraw_LPCNR_0016[7],s5k3p8spmipiraw_LPCNR_0024[0],s5k3p8spmipiraw_LPCNR_0024[1],s5k3p8spmipiraw_LPCNR_0024[2],s5k3p8spmipiraw_LPCNR_0024[3],s5k3p8spmipiraw_LPCNR_0024[4],s5k3p8spmipiraw_LPCNR_0024[5],
    s5k3p8spmipiraw_LPCNR_0024[6],s5k3p8spmipiraw_LPCNR_0024[7],
},
.FUS = {
    s5k3p8spmipiraw_FUS_0000[0],s5k3p8spmipiraw_FUS_0000[1],s5k3p8spmipiraw_FUS_0000[2],s5k3p8spmipiraw_FUS_0000[3],s5k3p8spmipiraw_FUS_0000[4],s5k3p8spmipiraw_FUS_0000[5],s5k3p8spmipiraw_FUS_0000[6],s5k3p8spmipiraw_FUS_0000[7],
},
.ZFUS = {
    s5k3p8spmipiraw_ZFUS_0000[0],s5k3p8spmipiraw_ZFUS_0000[1],s5k3p8spmipiraw_ZFUS_0000[2],s5k3p8spmipiraw_ZFUS_0000[3],s5k3p8spmipiraw_ZFUS_0000[4],s5k3p8spmipiraw_ZFUS_0000[5],s5k3p8spmipiraw_ZFUS_0000[6],s5k3p8spmipiraw_ZFUS_0000[7],
},
.MFB = {
    s5k3p8spmipiraw_MFB_0000[0],s5k3p8spmipiraw_MFB_0000[1],s5k3p8spmipiraw_MFB_0000[2],s5k3p8spmipiraw_MFB_0000[3],s5k3p8spmipiraw_MFB_0000[4],s5k3p8spmipiraw_MFB_0000[5],s5k3p8spmipiraw_MFB_0000[6],s5k3p8spmipiraw_MFB_0000[7],s5k3p8spmipiraw_MFB_0008[0],s5k3p8spmipiraw_MFB_0008[1],
    s5k3p8spmipiraw_MFB_0008[2],s5k3p8spmipiraw_MFB_0008[3],s5k3p8spmipiraw_MFB_0008[4],s5k3p8spmipiraw_MFB_0008[5],s5k3p8spmipiraw_MFB_0008[6],s5k3p8spmipiraw_MFB_0008[7],
},
.MIX = {
    s5k3p8spmipiraw_MIX_0000[0],s5k3p8spmipiraw_MIX_0000[1],s5k3p8spmipiraw_MIX_0000[2],s5k3p8spmipiraw_MIX_0000[3],s5k3p8spmipiraw_MIX_0000[4],s5k3p8spmipiraw_MIX_0000[5],s5k3p8spmipiraw_MIX_0000[6],s5k3p8spmipiraw_MIX_0000[7],s5k3p8spmipiraw_MIX_0008[0],s5k3p8spmipiraw_MIX_0008[1],
    s5k3p8spmipiraw_MIX_0008[2],s5k3p8spmipiraw_MIX_0008[3],s5k3p8spmipiraw_MIX_0008[4],s5k3p8spmipiraw_MIX_0008[5],s5k3p8spmipiraw_MIX_0008[6],s5k3p8spmipiraw_MIX_0008[7],
},
.YNRS = {
    s5k3p8spmipiraw_YNRS_0000[0],s5k3p8spmipiraw_YNRS_0000[1],s5k3p8spmipiraw_YNRS_0000[2],s5k3p8spmipiraw_YNRS_0000[3],s5k3p8spmipiraw_YNRS_0000[4],s5k3p8spmipiraw_YNRS_0000[5],s5k3p8spmipiraw_YNRS_0000[6],s5k3p8spmipiraw_YNRS_0000[7],
},
.YNR_TBL = {
    s5k3p8spmipiraw_YNR_TBL_0000[0],s5k3p8spmipiraw_YNR_TBL_0000[1],s5k3p8spmipiraw_YNR_TBL_0000[2],s5k3p8spmipiraw_YNR_TBL_0000[3],s5k3p8spmipiraw_YNR_TBL_0000[4],s5k3p8spmipiraw_YNR_TBL_0000[5],s5k3p8spmipiraw_YNR_TBL_0000[6],s5k3p8spmipiraw_YNR_TBL_0000[7],s5k3p8spmipiraw_YNR_TBL_0008[0],s5k3p8spmipiraw_YNR_TBL_0008[1],
    s5k3p8spmipiraw_YNR_TBL_0008[2],s5k3p8spmipiraw_YNR_TBL_0008[3],s5k3p8spmipiraw_YNR_TBL_0008[4],s5k3p8spmipiraw_YNR_TBL_0008[5],s5k3p8spmipiraw_YNR_TBL_0008[6],s5k3p8spmipiraw_YNR_TBL_0008[7],
},

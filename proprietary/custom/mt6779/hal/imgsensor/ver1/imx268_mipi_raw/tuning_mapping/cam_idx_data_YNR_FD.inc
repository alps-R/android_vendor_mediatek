#define IDX_DATA_YNR_FD_DIM_NS    4
#define IDX_DATA_YNR_FD_FACTOR_SZ    3
#define IDX_DATA_YNR_FD_ENTRY_NS    35

static unsigned int _cam_data_entry_YNR_FD_key0000[] = {0X000001FC, 0XA0000000, 0X00001F00, };
static unsigned int _cam_data_entry_YNR_FD_key0001[] = {0X00000000, 0X80100000, 0X00001F00, };
static unsigned int _cam_data_entry_YNR_FD_key0002[] = {0X00000000, 0X80400000, 0X00001F00, };
static unsigned int _cam_data_entry_YNR_FD_key0003[] = {0X000001FC, 0X60000000, 0X00001F00, };
static unsigned int _cam_data_entry_YNR_FD_key0004[] = {0X00000000, 0X40100000, 0X00001F00, };
static unsigned int _cam_data_entry_YNR_FD_key0005[] = {0X00000000, 0X40400000, 0X00001F00, };
static unsigned int _cam_data_entry_YNR_FD_key0006[] = {0X000001FC, 0XA0000000, 0X00002F00, };
static unsigned int _cam_data_entry_YNR_FD_key0007[] = {0X00000000, 0X80100000, 0X00002F00, };
static unsigned int _cam_data_entry_YNR_FD_key0008[] = {0X00000000, 0X80400000, 0X00002F00, };
static unsigned int _cam_data_entry_YNR_FD_key0009[] = {0X000001FC, 0X60000000, 0X00002F00, };
static unsigned int _cam_data_entry_YNR_FD_key0010[] = {0X00000000, 0X40100000, 0X00002F00, };
static unsigned int _cam_data_entry_YNR_FD_key0011[] = {0X00000000, 0X40400000, 0X00002F00, };
static unsigned int _cam_data_entry_YNR_FD_key0012[] = {0X00000001, 0X40040000, 0X00001F00, };
static unsigned int _cam_data_entry_YNR_FD_key0013[] = {0X00000001, 0X80040000, 0X00001100, };
static unsigned int _cam_data_entry_YNR_FD_key0014[] = {0X00000001, 0X00040000, 0X00001F01, };
static unsigned int _cam_data_entry_YNR_FD_key0015[] = {0X00000001, 0X40040000, 0X00002F00, };
static unsigned int _cam_data_entry_YNR_FD_key0016[] = {0X00000001, 0X80040000, 0X00002100, };
static unsigned int _cam_data_entry_YNR_FD_key0017[] = {0X00000001, 0X00040000, 0X00002F01, };
static unsigned int _cam_data_entry_YNR_FD_key0018[] = {0X00000002, 0X40080000, 0X00003F00, };
static unsigned int _cam_data_entry_YNR_FD_key0019[] = {0X00000002, 0X00080000, 0X00003F01, };
static unsigned int _cam_data_entry_YNR_FD_key0020[] = {0X00000000, 0XC0000330, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0021[] = {0X00000000, 0XC0000CC0, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0022[] = {0X00000000, 0XC003F000, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0023[] = {0X00CC0000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0024[] = {0X03300000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0025[] = {0XFC000000, 0XC000000F, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0026[] = {0X00000600, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0027[] = {0X00001800, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_YNR_FD_key0028[] = {0X0001E000, 0XC0000000, 0X000031FF, };
static unsigned int _cam_data_entry_YNR_FD_key0029[] = {0X0001E1FC, 0XE0000000, 0X000036FF, };
static unsigned int _cam_data_entry_YNR_FD_key0030[] = {0X00000000, 0XC0100000, 0X000036FF, };
static unsigned int _cam_data_entry_YNR_FD_key0031[] = {0X00000000, 0XC0400000, 0X000036FF, };
static unsigned int _cam_data_entry_YNR_FD_key0032[] = {0XFC000000, 0XC003F00F, 0X000036FF, };
static unsigned int _cam_data_entry_YNR_FD_key0033[] = {0X00000001, 0X80040000, 0X00001600, };
static unsigned int _cam_data_entry_YNR_FD_key0034[] = {0X00000001, 0X80040000, 0X00002600, };

static IDX_MASK_ENTRY _cam_data_entry_YNR_FD[IDX_DATA_YNR_FD_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0001, 8, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0002, 16, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0003, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0004, 8, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0005, 16, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0006, 24, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0007, 8, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0008, 16, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0009, 24, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0010, 8, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0011, 16, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0012, 32, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0013, 32, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0014, 32, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0015, 32, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0016, 32, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0017, 32, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0018, 40, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0019, 40, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0020, 32, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0021, 40, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0022, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0023, 32, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0024, 40, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0025, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0026, 32, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0027, 40, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0028, 0, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0029, 0, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0030, 8, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0031, 16, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0032, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0033, 32, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_YNR_FD_key0034, 32, 24, 0, 0},
};

static unsigned short _cam_data_dims_YNR_FD[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
};

static unsigned short _cam_data_expand_YNR_FD[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_YNR_FD =
{
    {IDX_ALGO_MASK, IDX_DATA_YNR_FD_DIM_NS, (unsigned short*)&_cam_data_dims_YNR_FD, (unsigned short*)&_cam_data_expand_YNR_FD},
    {IDX_DATA_YNR_FD_ENTRY_NS, IDX_DATA_YNR_FD_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_YNR_FD}
};
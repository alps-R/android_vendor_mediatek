#define IDX_DATA_BPC_PDC_DIM_NS    6
#define IDX_DATA_BPC_PDC_FACTOR_SZ    3
#define IDX_DATA_BPC_PDC_ENTRY_NS    26

static unsigned int _cam_data_entry_BPC_PDC_key0000[] = {0X000001FC, 0X80700000, 0X00016700, };
static unsigned int _cam_data_entry_BPC_PDC_key0001[] = {0X000001FC, 0X40700000, 0X00016700, };
static unsigned int _cam_data_entry_BPC_PDC_key0002[] = {0X000001FC, 0X80700000, 0X0001A700, };
static unsigned int _cam_data_entry_BPC_PDC_key0003[] = {0X000001FC, 0X40700000, 0X0001A700, };
static unsigned int _cam_data_entry_BPC_PDC_key0004[] = {0X00000001, 0X40040000, 0X000F7D02, };
static unsigned int _cam_data_entry_BPC_PDC_key0005[] = {0X00000001, 0X80040000, 0X00016500, };
static unsigned int _cam_data_entry_BPC_PDC_key0006[] = {0X00000001, 0X00040000, 0X000F7D01, };
static unsigned int _cam_data_entry_BPC_PDC_key0007[] = {0X00000001, 0X40040000, 0X000FBD02, };
static unsigned int _cam_data_entry_BPC_PDC_key0008[] = {0X00000001, 0X80040000, 0X0001A500, };
static unsigned int _cam_data_entry_BPC_PDC_key0009[] = {0X00000001, 0X00040000, 0X000FBD01, };
static unsigned int _cam_data_entry_BPC_PDC_key0010[] = {0X00000002, 0X40080000, 0X000FFD02, };
static unsigned int _cam_data_entry_BPC_PDC_key0011[] = {0X00000002, 0X00080000, 0X000FFD01, };
static unsigned int _cam_data_entry_BPC_PDC_key0012[] = {0X00CC0000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_BPC_PDC_key0013[] = {0X03300000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_BPC_PDC_key0014[] = {0XFC000000, 0XC000000F, 0X000FC7FF, };
static unsigned int _cam_data_entry_BPC_PDC_key0015[] = {0X00000600, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_BPC_PDC_key0016[] = {0X00001800, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_BPC_PDC_key0017[] = {0X0001E000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_BPC_PDC_key0018[] = {0X0001E1FC, 0XC0700000, 0X000FDBFF, };
static unsigned int _cam_data_entry_BPC_PDC_key0019[] = {0X00000001, 0X80040000, 0X00015900, };
static unsigned int _cam_data_entry_BPC_PDC_key0020[] = {0X00000001, 0X80040000, 0X00019900, };
static unsigned int _cam_data_entry_BPC_PDC_key0021[] = {0X000001FC, 0X80700000, 0X0006FF00, };
static unsigned int _cam_data_entry_BPC_PDC_key0022[] = {0X00000001, 0X80040000, 0X0006FF00, };
static unsigned int _cam_data_entry_BPC_PDC_key0023[] = {0XFFFFFFFF, 0XBFFFFFFF, 0X000FFF00, };
static unsigned int _cam_data_entry_BPC_PDC_key0024[] = {0XFFFFFFFF, 0X7FFFFFFF, 0X000FFFF8, };
static unsigned int _cam_data_entry_BPC_PDC_key0025[] = {0XFFFFFFFF, 0X3FFFFFFF, 0X000FFF07, };

static IDX_MASK_ENTRY _cam_data_entry_BPC_PDC[IDX_DATA_BPC_PDC_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0001, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0002, 0, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0003, 0, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0004, 8, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0005, 8, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0006, 16, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0007, 8, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0008, 8, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0009, 16, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0010, 24, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0011, 24, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0012, 8, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0013, 24, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0014, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0015, 8, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0016, 24, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0017, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0018, 0, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0019, 8, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0020, 8, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0021, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0022, 8, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0023, 0, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0024, 8, 25, 0, 0},
    {(unsigned int*)&_cam_data_entry_BPC_PDC_key0025, 24, 26, 0, 0},
};

static unsigned short _cam_data_dims_BPC_PDC[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_Zoom,
};

static unsigned short _cam_data_expand_BPC_PDC[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_BPC_PDC =
{
    {IDX_ALGO_MASK, IDX_DATA_BPC_PDC_DIM_NS, (unsigned short*)&_cam_data_dims_BPC_PDC, (unsigned short*)&_cam_data_expand_BPC_PDC},
    {IDX_DATA_BPC_PDC_ENTRY_NS, IDX_DATA_BPC_PDC_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_BPC_PDC}
};
#define IDX_DATA_GMA_DIM_NS    6
#define IDX_DATA_GMA_FACTOR_SZ    3
#define IDX_DATA_GMA_ENTRY_NS    27

static unsigned int _cam_data_entry_GMA_key0000[] = {0X000001FC, 0X80700000, 0X00016700, };
static unsigned int _cam_data_entry_GMA_key0001[] = {0X000001FC, 0X40700000, 0X00016700, };
static unsigned int _cam_data_entry_GMA_key0002[] = {0X000001FC, 0X80700000, 0X0001A700, };
static unsigned int _cam_data_entry_GMA_key0003[] = {0X000001FC, 0X40700000, 0X0001A700, };
static unsigned int _cam_data_entry_GMA_key0004[] = {0X00000001, 0X40040000, 0X000F7D02, };
static unsigned int _cam_data_entry_GMA_key0005[] = {0X00000001, 0X80040000, 0X00016500, };
static unsigned int _cam_data_entry_GMA_key0006[] = {0X00000001, 0X00040000, 0X000F7D01, };
static unsigned int _cam_data_entry_GMA_key0007[] = {0X00000001, 0X40040000, 0X000FBD02, };
static unsigned int _cam_data_entry_GMA_key0008[] = {0X00000001, 0X80040000, 0X0001A500, };
static unsigned int _cam_data_entry_GMA_key0009[] = {0X00000001, 0X00040000, 0X000FBD01, };
static unsigned int _cam_data_entry_GMA_key0010[] = {0X00000002, 0X40080000, 0X000FFD02, };
static unsigned int _cam_data_entry_GMA_key0011[] = {0X00000002, 0X00080000, 0X000FFD01, };
static unsigned int _cam_data_entry_GMA_key0012[] = {0X00CC0000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_GMA_key0013[] = {0X03300000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_GMA_key0014[] = {0XFC000000, 0XC000000F, 0X000FC7FF, };
static unsigned int _cam_data_entry_GMA_key0015[] = {0X00000600, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_GMA_key0016[] = {0X00001800, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_GMA_key0017[] = {0X0001E000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_GMA_key0018[] = {0X0001E1FC, 0XC0700000, 0X000FDBFF, };
static unsigned int _cam_data_entry_GMA_key0019[] = {0X00000001, 0X80040000, 0X00015900, };
static unsigned int _cam_data_entry_GMA_key0020[] = {0X00000001, 0X80040000, 0X00019900, };
static unsigned int _cam_data_entry_GMA_key0021[] = {0X00000000, 0XC3800000, 0X000FFFFF, };
static unsigned int _cam_data_entry_GMA_key0022[] = {0X000001FC, 0X80700000, 0X0006FF00, };
static unsigned int _cam_data_entry_GMA_key0023[] = {0X00000001, 0X80040000, 0X0006FF00, };
static unsigned int _cam_data_entry_GMA_key0024[] = {0XFFFFFFFF, 0XBFFFFFFF, 0X000FFF00, };
static unsigned int _cam_data_entry_GMA_key0025[] = {0XFFFFFFFF, 0X7FFFFFFF, 0X000FFFF8, };
static unsigned int _cam_data_entry_GMA_key0026[] = {0XFFFFFFFF, 0X3FFFFFFF, 0X000FFF07, };

static IDX_MASK_ENTRY _cam_data_entry_GMA[IDX_DATA_GMA_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_GMA_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0001, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0002, 0, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0003, 0, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0004, 1, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0005, 1, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0006, 2, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0007, 1, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0008, 1, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0009, 2, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0010, 3, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0011, 3, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0012, 4, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0013, 5, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0014, 6, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0015, 1, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0016, 3, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0017, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0018, 7, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0019, 1, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0020, 1, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0021, 0, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0022, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0023, 1, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0024, 0, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0025, 1, 25, 0, 0},
    {(unsigned int*)&_cam_data_entry_GMA_key0026, 3, 26, 0, 0},
};

static unsigned short _cam_data_dims_GMA[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_Zoom,
};

static unsigned short _cam_data_expand_GMA[] = 
{0, 0, 0};

const IDX_MASK_T cam_data_GMA =
{
    {IDX_ALGO_MASK, IDX_DATA_GMA_DIM_NS, (unsigned short*)&_cam_data_dims_GMA, (unsigned short*)&_cam_data_expand_GMA},
    {IDX_DATA_GMA_ENTRY_NS, IDX_DATA_GMA_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_GMA}
};
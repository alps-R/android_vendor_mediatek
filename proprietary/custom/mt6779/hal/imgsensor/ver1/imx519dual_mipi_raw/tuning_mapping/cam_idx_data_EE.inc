#define IDX_DATA_EE_DIM_NS    6
#define IDX_DATA_EE_FACTOR_SZ    3
#define IDX_DATA_EE_ENTRY_NS    56

static unsigned int _cam_data_entry_EE_key0000[] = {0X0000000C, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0001[] = {0X00000010, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0002[] = {0X00000100, 0X80000000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0003[] = {0X00000000, 0X80300000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0004[] = {0X00000000, 0X80400000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0005[] = {0X0000000C, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0006[] = {0X00000010, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0007[] = {0X00000100, 0X40000000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0008[] = {0X00000000, 0X40300000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0009[] = {0X00000000, 0X40400000, 0X00016700, };
static unsigned int _cam_data_entry_EE_key0010[] = {0X0000000C, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0011[] = {0X00000010, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0012[] = {0X00000100, 0X80000000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0013[] = {0X00000000, 0X80300000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0014[] = {0X00000000, 0X80400000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0015[] = {0X0000000C, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0016[] = {0X00000010, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0017[] = {0X00000100, 0X40000000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0018[] = {0X00000000, 0X40300000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0019[] = {0X00000000, 0X40400000, 0X0001A700, };
static unsigned int _cam_data_entry_EE_key0020[] = {0X00000001, 0X40040000, 0X000F7D02, };
static unsigned int _cam_data_entry_EE_key0021[] = {0X00000001, 0X80040000, 0X00016500, };
static unsigned int _cam_data_entry_EE_key0022[] = {0X00000001, 0X00040000, 0X000F7D01, };
static unsigned int _cam_data_entry_EE_key0023[] = {0X00000001, 0X40040000, 0X000FBD02, };
static unsigned int _cam_data_entry_EE_key0024[] = {0X00000001, 0X80040000, 0X0001A500, };
static unsigned int _cam_data_entry_EE_key0025[] = {0X00000001, 0X00040000, 0X000FBD01, };
static unsigned int _cam_data_entry_EE_key0026[] = {0X00000002, 0X40080000, 0X000FFD02, };
static unsigned int _cam_data_entry_EE_key0027[] = {0X00000002, 0X00080000, 0X000FFD01, };
static unsigned int _cam_data_entry_EE_key0028[] = {0X00CC0000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_EE_key0029[] = {0X03300000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_EE_key0030[] = {0X6C000000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_EE_key0031[] = {0X90000000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_EE_key0032[] = {0X00000000, 0XC0000008, 0X000FC7FF, };
static unsigned int _cam_data_entry_EE_key0033[] = {0X00000600, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_EE_key0034[] = {0X00001800, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_EE_key0035[] = {0X0001E000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_EE_key0036[] = {0X0001E00C, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_EE_key0037[] = {0X00000010, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_EE_key0038[] = {0X00000100, 0XC0000000, 0X000FDBFF, };
static unsigned int _cam_data_entry_EE_key0039[] = {0X00000000, 0XC0300000, 0X000FDBFF, };
static unsigned int _cam_data_entry_EE_key0040[] = {0X00000000, 0XC0400000, 0X000FDBFF, };
static unsigned int _cam_data_entry_EE_key0041[] = {0X00000001, 0X80040000, 0X00015900, };
static unsigned int _cam_data_entry_EE_key0042[] = {0X00000001, 0X80040000, 0X00019900, };
static unsigned int _cam_data_entry_EE_key0043[] = {0X0000000C, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_EE_key0044[] = {0X00000010, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_EE_key0045[] = {0X00000100, 0X80000000, 0X0006FF00, };
static unsigned int _cam_data_entry_EE_key0046[] = {0X00000000, 0X80300000, 0X0006FF00, };
static unsigned int _cam_data_entry_EE_key0047[] = {0X00000000, 0X80400000, 0X0006FF00, };
static unsigned int _cam_data_entry_EE_key0048[] = {0X00000001, 0X80040000, 0X0006FF00, };
static unsigned int _cam_data_entry_EE_key0049[] = {0XFFFFFFFF, 0XBFFFFFFF, 0X000FFF00, };
static unsigned int _cam_data_entry_EE_key0050[] = {0X90000010, 0X80024000, 0X000FFF00, };
static unsigned int _cam_data_entry_EE_key0051[] = {0X00000100, 0X80000008, 0X000FFF00, };
static unsigned int _cam_data_entry_EE_key0052[] = {0X00000000, 0X80300000, 0X000FFF00, };
static unsigned int _cam_data_entry_EE_key0053[] = {0X00000000, 0X80400000, 0X000FFF00, };
static unsigned int _cam_data_entry_EE_key0054[] = {0XFFFFFFFF, 0X7FFFFFFF, 0X000FFFF8, };
static unsigned int _cam_data_entry_EE_key0055[] = {0XFFFFFFFF, 0X3FFFFFFF, 0X000FFF07, };

static IDX_MASK_ENTRY _cam_data_entry_EE[IDX_DATA_EE_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_EE_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0001, 8, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0002, 16, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0003, 24, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0004, 32, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0005, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0006, 8, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0007, 16, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0008, 24, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0009, 32, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0010, 40, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0011, 48, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0012, 56, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0013, 24, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0014, 32, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0015, 40, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0016, 48, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0017, 56, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0018, 24, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0019, 32, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0020, 64, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0021, 64, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0022, 72, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0023, 64, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0024, 64, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0025, 72, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0026, 80, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0027, 80, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0028, 64, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0029, 80, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0030, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0031, 8, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0032, 16, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0033, 64, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0034, 80, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0035, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0036, 0, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0037, 8, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0038, 16, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0039, 24, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0040, 32, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0041, 64, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0042, 64, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0043, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0044, 8, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0045, 16, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0046, 24, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0047, 32, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0048, 64, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0049, 0, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0050, 8, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0051, 16, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0052, 24, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0053, 32, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0054, 64, 25, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0055, 80, 26, 0, 0},
};

static unsigned short _cam_data_dims_EE[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_Zoom,
};

static unsigned short _cam_data_expand_EE[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_EE =
{
    {IDX_ALGO_MASK, IDX_DATA_EE_DIM_NS, (unsigned short*)&_cam_data_dims_EE, (unsigned short*)&_cam_data_expand_EE},
    {IDX_DATA_EE_ENTRY_NS, IDX_DATA_EE_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_EE}
};
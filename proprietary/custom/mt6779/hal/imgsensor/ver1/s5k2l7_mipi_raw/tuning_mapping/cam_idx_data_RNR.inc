#define IDX_DATA_RNR_DIM_NS    5
#define IDX_DATA_RNR_FACTOR_SZ    4
#define IDX_DATA_RNR_ENTRY_NS    43

static unsigned int _cam_data_entry_RNR_key0000[] = {0X0000007C, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_RNR_key0001[] = {0X0000007C, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_RNR_key0002[] = {0X0000007C, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_RNR_key0003[] = {0X0000007C, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_RNR_key0004[] = {0X0000007C, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_RNR_key0005[] = {0X0000007C, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_RNR_key0006[] = {0X0000007C, 0X00000000, 0X00800000, 0X00000617, };
static unsigned int _cam_data_entry_RNR_key0007[] = {0X0000007C, 0X00000000, 0X00800000, 0X00000037, };
static unsigned int _cam_data_entry_RNR_key0008[] = {0X0000007C, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_RNR_key0009[] = {0X0000007C, 0X00000000, 0X00400000, 0X00000617, };
static unsigned int _cam_data_entry_RNR_key0010[] = {0X0000007C, 0X00000000, 0X00400000, 0X00000037, };
static unsigned int _cam_data_entry_RNR_key0011[] = {0X0000007C, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_RNR_key0012[] = {0X00000001, 0X00000000, 0X00400040, 0X000007EF, };
static unsigned int _cam_data_entry_RNR_key0013[] = {0X00000001, 0X00000000, 0X00800040, 0X000007E9, };
static unsigned int _cam_data_entry_RNR_key0014[] = {0X00000001, 0X00000000, 0X01000040, 0X000007EF, };
static unsigned int _cam_data_entry_RNR_key0015[] = {0X00000001, 0X00000000, 0X00400040, 0X000007F7, };
static unsigned int _cam_data_entry_RNR_key0016[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F1, };
static unsigned int _cam_data_entry_RNR_key0017[] = {0X00000001, 0X00000000, 0X01000040, 0X000007F7, };
static unsigned int _cam_data_entry_RNR_key0018[] = {0X00000002, 0X00000000, 0X00400080, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0019[] = {0X00000002, 0X00000000, 0X01000080, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0020[] = {0X00000000, 0X0000071C, 0XFFC1C000, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0021[] = {0X00000000, 0X000038E0, 0XFFCE0000, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0022[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_RNR_key0023[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_RNR_key0024[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0025[] = {0X01C70000, 0X00000000, 0XFFC00700, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0026[] = {0X0E380000, 0X00000000, 0XFFC03800, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0027[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_RNR_key0028[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_RNR_key0029[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0030[] = {0X00000300, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0031[] = {0X00000C00, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_RNR_key0032[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000619, };
static unsigned int _cam_data_entry_RNR_key0033[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000039, };
static unsigned int _cam_data_entry_RNR_key0034[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X000007F9, };
static unsigned int _cam_data_entry_RNR_key0035[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_RNR_key0036[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_RNR_key0037[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_RNR_key0038[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_RNR_key0039[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_RNR_key0040[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_RNR_key0041[] = {0X00000001, 0X00000000, 0X00800040, 0X000007EA, };
static unsigned int _cam_data_entry_RNR_key0042[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F2, };

static IDX_MASK_ENTRY _cam_data_entry_RNR[IDX_DATA_RNR_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_RNR_key0000, 0, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0001, 11, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0002, 1, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0003, 0, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0004, 11, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0005, 1, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0006, 0, 2, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0007, 11, 2, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0008, 1, 2, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0009, 0, 3, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0010, 11, 3, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0011, 1, 3, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0012, 12, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0013, 12, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0014, 12, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0015, 12, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0016, 12, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0017, 12, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0018, 20, 10, 2, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0019, 20, 11, 2, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0020, 12, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0021, 20, 13, 2, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0022, 0, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0023, 11, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0024, 1, 14, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0025, 12, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0026, 20, 16, 2, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0027, 0, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0028, 11, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0029, 1, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0030, 12, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0031, 20, 19, 2, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0032, 0, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0033, 11, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0034, 1, 20, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0035, 0, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0036, 11, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0037, 1, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0038, 0, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0039, 11, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_RNR_key0040, 1, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0041, 12, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_RNR_key0042, 12, 24, 0, 0},
};

static unsigned short _cam_data_dims_RNR[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_LV,
};

static unsigned short _cam_data_expand_RNR[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_RNR =
{
    {IDX_ALGO_MASK, IDX_DATA_RNR_DIM_NS, (unsigned short*)&_cam_data_dims_RNR, (unsigned short*)&_cam_data_expand_RNR},
    {IDX_DATA_RNR_ENTRY_NS, IDX_DATA_RNR_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_RNR}
};
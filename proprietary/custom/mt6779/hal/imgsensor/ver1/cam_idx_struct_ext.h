#ifndef _CAM_IDX_STRUCT_EXT_H_
#define _CAM_IDX_STRUCT_EXT_H_

namespace NSIspTuning{
#define Comm_EIspProfile_Preview 0
#define Comm_EIspProfile_Video 1
#define Comm_EIspProfile_Capture 2
#define Comm_EIspProfile_Capture_DCE 3
#define Comm_EIspProfile_Capture_DSDN 4
#define Comm_EIspProfile_MFNR_Before_Blend 5
#define Comm_EIspProfile_MFNR_Single 6
#define Comm_EIspProfile_MFNR_MFB 7
#define Comm_EIspProfile_MFNR_After_Blend 8
#define Comm_EIspProfile_N3D_Preview 9
#define Comm_EIspProfile_N3D_Preview_toW 10
#define Comm_EIspProfile_N3D_Video 11
#define Comm_EIspProfile_N3D_Video_toW 12
#define Comm_EIspProfile_N3D_Capture 13
#define Comm_EIspProfile_N3D_Capture_Full 14
#define Comm_EIspProfile_N3D_Capture_Depth 15
#define Comm_EIspProfile_N3D_Capture_Depth_toW 16
#define Comm_EIspProfile_Bokeh 17
#define Comm_EIspProfile_zHDR_Preview 18
#define Comm_EIspProfile_mHDR_Preview 19
#define Comm_EIspProfile_zHDR_Video 20
#define Comm_EIspProfile_mHDR_Video 21
#define Comm_EIspProfile_zHDR_Preview_VSS 22
#define Comm_EIspProfile_mHDR_Preview_VSS 23
#define Comm_EIspProfile_zHDR_Video_VSS 24
#define Comm_EIspProfile_mHDR_Video_VSS 25
#define Comm_EIspProfile_zHDR_Capture 26
#define Comm_EIspProfile_zHDR_Capture_DCE 27
#define Comm_EIspProfile_zHDR_Capture_DSDN 28
#define Comm_EIspProfile_mHDR_Capture 29
#define Comm_EIspProfile_mHDR_Capture_DCE 30
#define Comm_EIspProfile_mHDR_Capture_DSDN 31
#define Comm_EIspProfile_zHDR_Capture_MFNR_Before_Blend 32
#define Comm_EIspProfile_zHDR_Capture_MFNR_Single 33
#define Comm_EIspProfile_zHDR_Capture_MFNR_MFB 34
#define Comm_EIspProfile_zHDR_Capture_MFNR_After_Blend 35
#define Comm_EIspProfile_Auto_zHDR_Preview 36
#define Comm_EIspProfile_Auto_mHDR_Preview 37
#define Comm_EIspProfile_Auto_zHDR_Video 38
#define Comm_EIspProfile_Auto_mHDR_Video 39
#define Comm_EIspProfile_Auto_zHDR_Preview_VSS 40
#define Comm_EIspProfile_Auto_mHDR_Preview_VSS 41
#define Comm_EIspProfile_Auto_zHDR_Video_VSS 42
#define Comm_EIspProfile_Auto_mHDR_Video_VSS 43
#define Comm_EIspProfile_Auto_zHDR_Capture 44
#define Comm_EIspProfile_Auto_zHDR_Capture_DCE 45
#define Comm_EIspProfile_Auto_zHDR_Capture_DSDN 46
#define Comm_EIspProfile_Auto_mHDR_Capture 47
#define Comm_EIspProfile_Auto_mHDR_Capture_DCE 48
#define Comm_EIspProfile_Auto_mHDR_Capture_DSDN 49
#define Comm_EIspProfile_EIS_Preview 50
#define Comm_EIspProfile_EIS_Video 51
#define Comm_EIspProfile_AINR_Main 52
#define Comm_EIspProfile_AINR_MainYUV 53
#define Comm_EIspProfile_AINR_Single 54
#define Comm_EIspProfile_P1_YUV_HighRes 55
#define Comm_EIspProfile_P1_YUV_FD 56
#define Comm_EIspProfile_P1_YUV_Depth 57
#define Comm_EIspProfile_SWHDR_RAW_DOMAIN 58
#define Comm_EIspProfile_SWHDR_YUV_DOMAIN 59
#define Comm_EIspProfile_YUV_Reprocess 60
#define Comm_EIspProfile_3rd_Party_Processed_Raw_Reprocess 61
#define Comm_ESensorMode_Preview 0
#define Comm_ESensorMode_Capture 1
#define Comm_ESensorMode_Video 2
#define Comm_ESensorMode_SlimVideo1 3
#define Comm_ESensorMode_SlimVideo2 4
#define Comm_ESensorMode_Custom1 5
#define Comm_ESensorMode_Custom2 6
#define Comm_ESensorMode_Custom3 7
#define Comm_ESensorMode_Custom4 8
#define Comm_ESensorMode_Custom5 9
#define Comm_EFrontBin_No 0
#define Comm_EFrontBin_Yes 1
#define Comm_ESize_SIZE_00 0
#define Comm_ESize_SIZE_01 1
#define Comm_ESize_SIZE_02 2
#define Comm_ESize_SIZE_03 3
#define Comm_EFlash_No 0
#define Comm_EFlash_MainFlash 1
#define Comm_EFlash_PreFlash 2
#define Comm_EFlash_Torch 3
#define Comm_EApp_MTKCam 0
#define Comm_EApp_Facebook 1
#define Comm_EApp_Line 2
#define Comm_EApp_QQ 3
#define Comm_EApp_Wechat 4
#define Comm_EApp_Skype 5
#define Comm_EApp_Normal 6
#define Comm_EApp_Professional 7
#define Comm_EApp_FaceBeauty 8
#define Comm_EApp_HDR 9
#define Comm_EApp_Panorama 10
#define Comm_EApp_Video 11
#define Comm_EApp_3rd_party 12
#define Comm_EFaceDetection_No 0
#define Comm_EFaceDetection_Yes 1
#define Comm_EDriverIC_DriverIC_00 0
#define Comm_ECustom_00_Custom_00_00 0
#define Comm_ECustom_00_Custom_00_01 1
#define Comm_ECustom_00_Custom_00_02 2
#define Comm_ECustom_01_Custom_01_00 0
#define Comm_ECustom_01_Custom_01_01 1
#define Comm_ECustom_01_Custom_01_02 2
#define Comm_EZoom_IDX_00 0
#define Comm_EZoom_IDX_01 1
#define Comm_EZoom_IDX_02 2
#define Comm_EZoom_IDX_03 3
#define Comm_ELV_IDX_00 0
#define Comm_ELV_IDX_01 1
#define Comm_ELV_IDX_02 2
#define Comm_ELV_IDX_03 3
#define Comm_ELV_IDX_04 4
#define Comm_ELV_IDX_05 5
#define Comm_ECT_IDX_00 0
#define Comm_ECT_IDX_01 1
#define Comm_ECT_IDX_02 2
#define Comm_ECT_IDX_03 3
#define Comm_ECT_IDX_04 4
#define Comm_ECT_IDX_05 5
#define Comm_ECT_IDX_06 6
#define Comm_ECT_IDX_07 7
#define Comm_ECT_IDX_08 8
#define Comm_ECT_IDX_09 9
#define Comm_EISO_IDX_00 0
#define Comm_EISO_IDX_01 1
#define Comm_EISO_IDX_02 2
#define Comm_EISO_IDX_03 3
#define Comm_EISO_IDX_04 4
#define Comm_EISO_IDX_05 5
#define Comm_EISO_IDX_06 6
#define Comm_EISO_IDX_07 7
#define Comm_EISO_IDX_08 8
#define Comm_EISO_IDX_09 9
#define Comm_EISO_IDX_10 10
#define Comm_EISO_IDX_11 11
#define Comm_EISO_IDX_12 12
#define Comm_EISO_IDX_13 13
#define Comm_EISO_IDX_14 14
#define Comm_EISO_IDX_15 15
#define Comm_EISO_IDX_16 16
#define Comm_EISO_IDX_17 17
#define Comm_EISO_IDX_18 18
#define Comm_EISO_IDX_19 19


#define GEN_TOOL_EXT_TIMESTAMP "2018-12-27T14:01:33.931000"

typedef enum
{
    IDX_ALGO_MASK,
    IDX_ALGO_DM,
    IDX_ALGO_TREE,
    IDX_ALGO_HASH,
    IDX_ALGO_NUM
} IDX_ALGO_TYPE;

typedef enum
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Size,
    EDim_Flash,
    EDim_App,
    EDim_FaceDetection,
    EDim_DriverIC,
    EDim_Custom_00,
    EDim_Custom_01,
    EDim_Zoom,
    EDim_LV,
    EDim_CT,
    EDim_ISO,
    EDim_NUM
} EDim_T;

typedef enum
{
    EISO_GROUP_00,
    EISO_GROUP_01,
    EISO_GROUP_02,
    EISO_GROUP_03,
    EISO_GROUP_04,
    EISO_GROUP_05,
    EISO_GROUP_06,
    EISO_GROUP_07,
    EISO_GROUP_08,
    EISO_GROUP_09,
    EISO_GROUP_NUM
} EISO_GROUP_T;

#if MTK_CAM_NEW_NVRAM_SUPPORT
typedef enum
{
    EIspProfile_Preview = Comm_EIspProfile_Preview,
    EIspProfile_Video = Comm_EIspProfile_Video,
    EIspProfile_Capture = Comm_EIspProfile_Capture,
    EIspProfile_Capture_DCE = Comm_EIspProfile_Capture_DCE,
    EIspProfile_Capture_DSDN = Comm_EIspProfile_Capture_DSDN,
    EIspProfile_MFNR_Before_Blend = Comm_EIspProfile_MFNR_Before_Blend,
    EIspProfile_MFNR_Single = Comm_EIspProfile_MFNR_Single,
    EIspProfile_MFNR_MFB = Comm_EIspProfile_MFNR_MFB,
    EIspProfile_MFNR_After_Blend = Comm_EIspProfile_MFNR_After_Blend,
    EIspProfile_N3D_Preview = Comm_EIspProfile_N3D_Preview,
    EIspProfile_N3D_Preview_toW = Comm_EIspProfile_N3D_Preview_toW,
    EIspProfile_N3D_Video = Comm_EIspProfile_N3D_Video,
    EIspProfile_N3D_Video_toW = Comm_EIspProfile_N3D_Video_toW,
    EIspProfile_N3D_Capture = Comm_EIspProfile_N3D_Capture,
    EIspProfile_N3D_Capture_Full = Comm_EIspProfile_N3D_Capture_Full,
    EIspProfile_N3D_Capture_Depth = Comm_EIspProfile_N3D_Capture_Depth,
    EIspProfile_N3D_Capture_Depth_toW = Comm_EIspProfile_N3D_Capture_Depth_toW,
    EIspProfile_Bokeh = Comm_EIspProfile_Bokeh,
    EIspProfile_zHDR_Preview = Comm_EIspProfile_zHDR_Preview,
    EIspProfile_mHDR_Preview = Comm_EIspProfile_mHDR_Preview,
    EIspProfile_zHDR_Video = Comm_EIspProfile_zHDR_Video,
    EIspProfile_mHDR_Video = Comm_EIspProfile_mHDR_Video,
    EIspProfile_zHDR_Preview_VSS = Comm_EIspProfile_zHDR_Preview_VSS,
    EIspProfile_mHDR_Preview_VSS = Comm_EIspProfile_mHDR_Preview_VSS,
    EIspProfile_zHDR_Video_VSS = Comm_EIspProfile_zHDR_Video_VSS,
    EIspProfile_mHDR_Video_VSS = Comm_EIspProfile_mHDR_Video_VSS,
    EIspProfile_zHDR_Capture = Comm_EIspProfile_zHDR_Capture,
    EIspProfile_zHDR_Capture_DCE = Comm_EIspProfile_zHDR_Capture_DCE,
    EIspProfile_zHDR_Capture_DSDN = Comm_EIspProfile_zHDR_Capture_DSDN,
    EIspProfile_mHDR_Capture = Comm_EIspProfile_mHDR_Capture,
    EIspProfile_mHDR_Capture_DCE = Comm_EIspProfile_mHDR_Capture_DCE,
    EIspProfile_mHDR_Capture_DSDN = Comm_EIspProfile_mHDR_Capture_DSDN,
    EIspProfile_zHDR_Capture_MFNR_Before_Blend = Comm_EIspProfile_zHDR_Capture_MFNR_Before_Blend,
    EIspProfile_zHDR_Capture_MFNR_Single = Comm_EIspProfile_zHDR_Capture_MFNR_Single,
    EIspProfile_zHDR_Capture_MFNR_MFB = Comm_EIspProfile_zHDR_Capture_MFNR_MFB,
    EIspProfile_zHDR_Capture_MFNR_After_Blend = Comm_EIspProfile_zHDR_Capture_MFNR_After_Blend,
    EIspProfile_Auto_zHDR_Preview = Comm_EIspProfile_Auto_zHDR_Preview,
    EIspProfile_Auto_mHDR_Preview = Comm_EIspProfile_Auto_mHDR_Preview,
    EIspProfile_Auto_zHDR_Video = Comm_EIspProfile_Auto_zHDR_Video,
    EIspProfile_Auto_mHDR_Video = Comm_EIspProfile_Auto_mHDR_Video,
    EIspProfile_Auto_zHDR_Preview_VSS = Comm_EIspProfile_Auto_zHDR_Preview_VSS,
    EIspProfile_Auto_mHDR_Preview_VSS = Comm_EIspProfile_Auto_mHDR_Preview_VSS,
    EIspProfile_Auto_zHDR_Video_VSS = Comm_EIspProfile_Auto_zHDR_Video_VSS,
    EIspProfile_Auto_mHDR_Video_VSS = Comm_EIspProfile_Auto_mHDR_Video_VSS,
    EIspProfile_Auto_zHDR_Capture = Comm_EIspProfile_Auto_zHDR_Capture,
    EIspProfile_Auto_zHDR_Capture_DCE = Comm_EIspProfile_Auto_zHDR_Capture_DCE,
    EIspProfile_Auto_zHDR_Capture_DSDN = Comm_EIspProfile_Auto_zHDR_Capture_DSDN,
    EIspProfile_Auto_mHDR_Capture = Comm_EIspProfile_Auto_mHDR_Capture,
    EIspProfile_Auto_mHDR_Capture_DCE = Comm_EIspProfile_Auto_mHDR_Capture_DCE,
    EIspProfile_Auto_mHDR_Capture_DSDN = Comm_EIspProfile_Auto_mHDR_Capture_DSDN,
    EIspProfile_EIS_Preview = Comm_EIspProfile_EIS_Preview,
    EIspProfile_EIS_Video = Comm_EIspProfile_EIS_Video,
    EIspProfile_AINR_Main = Comm_EIspProfile_AINR_Main,
    EIspProfile_AINR_MainYUV = Comm_EIspProfile_AINR_MainYUV,
    EIspProfile_AINR_Single = Comm_EIspProfile_AINR_Single,
    EIspProfile_P1_YUV_HighRes = Comm_EIspProfile_P1_YUV_HighRes,
    EIspProfile_P1_YUV_FD = Comm_EIspProfile_P1_YUV_FD,
    EIspProfile_P1_YUV_Depth = Comm_EIspProfile_P1_YUV_Depth,
    EIspProfile_SWHDR_RAW_DOMAIN = Comm_EIspProfile_SWHDR_RAW_DOMAIN,
    EIspProfile_SWHDR_YUV_DOMAIN = Comm_EIspProfile_SWHDR_YUV_DOMAIN,
    EIspProfile_YUV_Reprocess = Comm_EIspProfile_YUV_Reprocess,
    EIspProfile_3rd_Party_Processed_Raw_Reprocess = Comm_EIspProfile_3rd_Party_Processed_Raw_Reprocess,
    EIspProfile_NUM,
    EIspProfile_Flash_Capture,
    EIspProfile_Preview_Capture_ZOOM1,
    EIspProfile_Preview_Capture_ZOOM2,
    EIspProfile_Video_Preview_ZOOM1,
    EIspProfile_Video_Preview_ZOOM2,
    EIspProfile_Video_Video_ZOOM1,
    EIspProfile_Video_Video_ZOOM2,
    EIspProfile_Capture_Capture_ZOOM1,
    EIspProfile_Capture_Capture_ZOOM2,
    EIspProfile_MFNR_Before_ZOOM0,
    EIspProfile_MFNR_Before_ZOOM1,
    EIspProfile_MFNR_Before_ZOOM2,
    EIspProfile_MFNR_Single_ZOOM0,
    EIspProfile_MFNR_Single_ZOOM1,
    EIspProfile_MFNR_Single_ZOOM2,
    EIspProfile_MFNR_MFB_ZOOM0,
    EIspProfile_MFNR_MFB_ZOOM1,
    EIspProfile_MFNR_MFB_ZOOM2,
    EIspProfile_MFNR_After_ZOOM0,
    EIspProfile_MFNR_After_ZOOM1,
    EIspProfile_MFNR_After_ZOOM2,
    EIspProfile_Flash_Capture_ZOOM1,
    EIspProfile_Flash_Capture_ZOOM2,
    EIspProfile_iHDR_Preview_LINE,
    EIspProfile_zHDR_Preview_LINE,
    EIspProfile_mHDR_Preview_LINE,
    EIspProfile_Preview_Capture_Facebook,
    EIspProfile_Preview_Video_Facebook,
    EIspProfile_Video_Video_Facebook,
    EIspProfile_iHDR_Video_Messenger,
    EIspProfile_zHDR_Video_Messenger,
    EIspProfile_mHDR_Video_Messenger,
    EIspProfile_iHDR_Video_WhatsApp,
    EIspProfile_zHDR_Video_WhatsApp,
    EIspProfile_mHDR_Video_WhatsApp,
    EIspProfile_Capture_Capture_WhatsApp,
    EIspProfile_N3D_Video_WhatsApp,
    EIspProfile_N3D_Preview_QQ,
    EIspProfile_Capture_Capture_QQ,
} EIspProfile_T;

#endif
typedef enum
{
    ESensorMode_Preview = Comm_ESensorMode_Preview,
    ESensorMode_Capture = Comm_ESensorMode_Capture,
    ESensorMode_Video = Comm_ESensorMode_Video,
    ESensorMode_SlimVideo1 = Comm_ESensorMode_SlimVideo1,
    ESensorMode_SlimVideo2 = Comm_ESensorMode_SlimVideo2,
    ESensorMode_Custom1 = Comm_ESensorMode_Custom1,
    ESensorMode_Custom2 = Comm_ESensorMode_Custom2,
    ESensorMode_Custom3 = Comm_ESensorMode_Custom3,
    ESensorMode_Custom4 = Comm_ESensorMode_Custom4,
    ESensorMode_Custom5 = Comm_ESensorMode_Custom5,
    ESensorMode_NUM,
} ESensorMode_T;

typedef enum
{
    EFrontBin_No = Comm_EFrontBin_No,
    EFrontBin_Yes = Comm_EFrontBin_Yes,
    EFrontBin_NUM,
} EFrontBin_T;

typedef enum
{
    ESize_SIZE_00 = Comm_ESize_SIZE_00,
    ESize_SIZE_01 = Comm_ESize_SIZE_01,
    ESize_SIZE_02 = Comm_ESize_SIZE_02,
    ESize_SIZE_03 = Comm_ESize_SIZE_03,
    ESize_NUM,
} ESize_T;

typedef enum
{
    EFlash_No = Comm_EFlash_No,
    EFlash_MainFlash = Comm_EFlash_MainFlash,
    EFlash_PreFlash = Comm_EFlash_PreFlash,
    EFlash_Torch = Comm_EFlash_Torch,
    EFlash_NUM,
} EFlash_T;

typedef enum
{
    EApp_MTKCam = Comm_EApp_MTKCam,
    EApp_Facebook = Comm_EApp_Facebook,
    EApp_Line = Comm_EApp_Line,
    EApp_QQ = Comm_EApp_QQ,
    EApp_Wechat = Comm_EApp_Wechat,
    EApp_Skype = Comm_EApp_Skype,
    EApp_Normal = Comm_EApp_Normal,
    EApp_Professional = Comm_EApp_Professional,
    EApp_FaceBeauty = Comm_EApp_FaceBeauty,
    EApp_HDR = Comm_EApp_HDR,
    EApp_Panorama = Comm_EApp_Panorama,
    EApp_Video = Comm_EApp_Video,
    EApp_3rd_party = Comm_EApp_3rd_party,
    EApp_NUM,
} EApp_T;

typedef enum
{
    EFaceDetection_No = Comm_EFaceDetection_No,
    EFaceDetection_Yes = Comm_EFaceDetection_Yes,
    EFaceDetection_NUM,
} EFaceDetection_T;

typedef enum
{
    EDriverIC_DriverIC_00 = Comm_EDriverIC_DriverIC_00,
    EDriverIC_NUM,
} EDriverIC_T;

typedef enum
{
    ECustom_00_Custom_00_00 = Comm_ECustom_00_Custom_00_00,
    ECustom_00_Custom_00_01 = Comm_ECustom_00_Custom_00_01,
    ECustom_00_Custom_00_02 = Comm_ECustom_00_Custom_00_02,
    ECustom_00_NUM,
} ECustom_00_T;

typedef enum
{
    ECustom_01_Custom_01_00 = Comm_ECustom_01_Custom_01_00,
    ECustom_01_Custom_01_01 = Comm_ECustom_01_Custom_01_01,
    ECustom_01_Custom_01_02 = Comm_ECustom_01_Custom_01_02,
    ECustom_01_NUM,
} ECustom_01_T;

typedef enum
{
    EZoom_IDX_00 = Comm_EZoom_IDX_00,
    EZoom_IDX_01 = Comm_EZoom_IDX_01,
    EZoom_IDX_02 = Comm_EZoom_IDX_02,
    EZoom_IDX_03 = Comm_EZoom_IDX_03,
    EZoom_NUM,
} EZoom_T;

typedef enum
{
    ELV_IDX_00 = Comm_ELV_IDX_00,
    ELV_IDX_01 = Comm_ELV_IDX_01,
    ELV_IDX_02 = Comm_ELV_IDX_02,
    ELV_IDX_03 = Comm_ELV_IDX_03,
    ELV_IDX_04 = Comm_ELV_IDX_04,
    ELV_IDX_05 = Comm_ELV_IDX_05,
    ELV_NUM,
} ELV_T;

typedef enum
{
    ECT_IDX_00 = Comm_ECT_IDX_00,
    ECT_IDX_01 = Comm_ECT_IDX_01,
    ECT_IDX_02 = Comm_ECT_IDX_02,
    ECT_IDX_03 = Comm_ECT_IDX_03,
    ECT_IDX_04 = Comm_ECT_IDX_04,
    ECT_IDX_05 = Comm_ECT_IDX_05,
    ECT_IDX_06 = Comm_ECT_IDX_06,
    ECT_IDX_07 = Comm_ECT_IDX_07,
    ECT_IDX_08 = Comm_ECT_IDX_08,
    ECT_IDX_09 = Comm_ECT_IDX_09,
    ECT_NUM,
} ECT_T;

typedef enum
{
    EISO_IDX_00 = Comm_EISO_IDX_00,
    EISO_IDX_01 = Comm_EISO_IDX_01,
    EISO_IDX_02 = Comm_EISO_IDX_02,
    EISO_IDX_03 = Comm_EISO_IDX_03,
    EISO_IDX_04 = Comm_EISO_IDX_04,
    EISO_IDX_05 = Comm_EISO_IDX_05,
    EISO_IDX_06 = Comm_EISO_IDX_06,
    EISO_IDX_07 = Comm_EISO_IDX_07,
    EISO_IDX_08 = Comm_EISO_IDX_08,
    EISO_IDX_09 = Comm_EISO_IDX_09,
    EISO_IDX_10 = Comm_EISO_IDX_10,
    EISO_IDX_11 = Comm_EISO_IDX_11,
    EISO_IDX_12 = Comm_EISO_IDX_12,
    EISO_IDX_13 = Comm_EISO_IDX_13,
    EISO_IDX_14 = Comm_EISO_IDX_14,
    EISO_IDX_15 = Comm_EISO_IDX_15,
    EISO_IDX_16 = Comm_EISO_IDX_16,
    EISO_IDX_17 = Comm_EISO_IDX_17,
    EISO_IDX_18 = Comm_EISO_IDX_18,
    EISO_IDX_19 = Comm_EISO_IDX_19,
    EISO_NUM,
} EISO_T;

typedef enum
{
    EModule_SLK,
    EModule_AE,
    EModule_AF,
    EModule_AWB,
    EModule_Flash_AE,
    EModule_Flash_AWB,
    EModule_Flash_Calibration,
    EModule_LCE,
    EModule_GMA,
    EModule_DCE,
    EModule_DM,
    EModule_YNR,
    EModule_YNR_LCE_LINK,
    EModule_YNR_FD,
    EModule_CNR_CNR,
    EModule_EE,
    EModule_NR3D,
    EModule_HFG,
    EModule_OBC,
    EModule_BPC_BPC,
    EModule_BPC_CT,
    EModule_BPC_PDC,
    EModule_LDNR,
    EModule_CNR_CCR,
    EModule_CNR_ABF,
    EModule_SWNR_THRES,
    EModule_SWNR,
    EModule_LPCNR,
    EModule_FUS,
    EModule_ZFUS,
    EModule_MFNR_THRES,
    EModule_MFNR,
    EModule_BSS,
    EModule_MFB,
    EModule_MIX,
    EModule_CA_LTM,
    EModule_CCM,
    EModule_COLOR_PARAM,
    EModule_COLOR,
    EModule_ClearZoom,
    EModule_AINR,
    EModule_DSDN,
    EModule_YNRS,
    EModule_YNR_TBL,
    EModule_LTM,
    EModule_HLR,
    EModule_AINR_THRES,
    EModule_NUM
} EModule_T;

}
#endif
#define IDX_DATA_EE_DIM_NS    4
#define IDX_DATA_EE_FACTOR_SZ    3
#define IDX_DATA_EE_ENTRY_NS    49

static unsigned int _cam_data_entry_EE_key0000[] = {0X0000000C, 0X80000000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0001[] = {0X00000010, 0X80000000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0002[] = {0X00000100, 0X80000000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0003[] = {0X00000000, 0X80300000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0004[] = {0X00000000, 0X80400000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0005[] = {0X0000000C, 0X40000000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0006[] = {0X00000010, 0X40000000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0007[] = {0X00000100, 0X40000000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0008[] = {0X00000000, 0X40300000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0009[] = {0X00000000, 0X40400000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0010[] = {0X0000000C, 0X80000000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0011[] = {0X00000010, 0X80000000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0012[] = {0X00000100, 0X80000000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0013[] = {0X00000000, 0X80300000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0014[] = {0X00000000, 0X80400000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0015[] = {0X0000000C, 0X40000000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0016[] = {0X00000010, 0X40000000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0017[] = {0X00000100, 0X40000000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0018[] = {0X00000000, 0X40300000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0019[] = {0X00000000, 0X40400000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0020[] = {0X00000001, 0X40040000, 0X00001F02, };
static unsigned int _cam_data_entry_EE_key0021[] = {0X00000001, 0X80040000, 0X00001100, };
static unsigned int _cam_data_entry_EE_key0022[] = {0X00000001, 0X00040000, 0X00001F01, };
static unsigned int _cam_data_entry_EE_key0023[] = {0X00000001, 0X40040000, 0X00002F00, };
static unsigned int _cam_data_entry_EE_key0024[] = {0X00000001, 0X80040000, 0X00002100, };
static unsigned int _cam_data_entry_EE_key0025[] = {0X00000001, 0X00040000, 0X00002F01, };
static unsigned int _cam_data_entry_EE_key0026[] = {0X00000002, 0X40080000, 0X00003F02, };
static unsigned int _cam_data_entry_EE_key0027[] = {0X00000002, 0X00080000, 0X00003F01, };
static unsigned int _cam_data_entry_EE_key0028[] = {0X00000000, 0XC0000330, 0X00003FFF, };
static unsigned int _cam_data_entry_EE_key0029[] = {0X00000000, 0XC0000CC0, 0X00003FFF, };
static unsigned int _cam_data_entry_EE_key0030[] = {0X00000000, 0XC001B000, 0X000031FF, };
static unsigned int _cam_data_entry_EE_key0031[] = {0X00CC0000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_EE_key0032[] = {0X03300000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_EE_key0033[] = {0X6C000000, 0XC0000000, 0X000031FF, };
static unsigned int _cam_data_entry_EE_key0034[] = {0X90000000, 0XC0000000, 0X000031FF, };
static unsigned int _cam_data_entry_EE_key0035[] = {0X00000000, 0XC0000008, 0X000031FF, };
static unsigned int _cam_data_entry_EE_key0036[] = {0X00000600, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_EE_key0037[] = {0X00001800, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_EE_key0038[] = {0X0001E000, 0XC0000000, 0X000031FF, };
static unsigned int _cam_data_entry_EE_key0039[] = {0X0001E00C, 0XC0000000, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0040[] = {0X00000010, 0XC0000000, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0041[] = {0X00000100, 0XC0000000, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0042[] = {0X00000000, 0XC0300000, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0043[] = {0X00000000, 0XC0400000, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0044[] = {0X6C000000, 0XC001B000, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0045[] = {0X90000000, 0XC0024000, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0046[] = {0X00000000, 0XC0000008, 0X000036FF, };
static unsigned int _cam_data_entry_EE_key0047[] = {0X00000001, 0X80040000, 0X00001600, };
static unsigned int _cam_data_entry_EE_key0048[] = {0X00000001, 0X80040000, 0X00002600, };

static IDX_MASK_ENTRY _cam_data_entry_EE[IDX_DATA_EE_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_EE_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0001, 8, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0002, 16, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0003, 24, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0004, 32, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0005, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0006, 8, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0007, 16, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0008, 24, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0009, 32, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0010, 40, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0011, 48, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0012, 56, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0013, 24, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0014, 32, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0015, 40, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0016, 48, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0017, 56, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0018, 24, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0019, 32, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0020, 64, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0021, 64, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0022, 64, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0023, 64, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0024, 64, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0025, 64, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0026, 72, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0027, 72, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0028, 64, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0029, 72, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0030, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0031, 64, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0032, 72, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0033, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0034, 8, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0035, 16, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0036, 64, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0037, 72, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0038, 0, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0039, 0, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0040, 8, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0041, 16, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0042, 24, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0043, 32, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0044, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0045, 8, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0046, 16, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0047, 64, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_EE_key0048, 64, 24, 0, 0},
};

static unsigned short _cam_data_dims_EE[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
};

static unsigned short _cam_data_expand_EE[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_EE =
{
    {IDX_ALGO_MASK, IDX_DATA_EE_DIM_NS, (unsigned short*)&_cam_data_dims_EE, (unsigned short*)&_cam_data_expand_EE},
    {IDX_DATA_EE_ENTRY_NS, IDX_DATA_EE_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_EE}
};
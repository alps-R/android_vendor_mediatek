#define IDX_DATA_NBC_LCE_LINK_DIM_NS    5
#define IDX_DATA_NBC_LCE_LINK_FACTOR_SZ    4
#define IDX_DATA_NBC_LCE_LINK_ENTRY_NS    39

static unsigned int _cam_data_entry_NBC_LCE_LINK_key0000[] = {0X0000007C, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0001[] = {0X0000007C, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0002[] = {0X0000007C, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0003[] = {0X0000007C, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0004[] = {0X0000007C, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0005[] = {0X0000007C, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0006[] = {0X0000007C, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0007[] = {0X0000007C, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0008[] = {0X00000001, 0X00000000, 0X00400040, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0009[] = {0X00000001, 0X00000000, 0X00800040, 0X000007E9, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0010[] = {0X00000001, 0X00000000, 0X01000040, 0X000007EF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0011[] = {0X00000001, 0X00000000, 0X00400040, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0012[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F1, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0013[] = {0X00000001, 0X00000000, 0X01000040, 0X000007F7, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0014[] = {0X00000002, 0X00000000, 0X00400080, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0015[] = {0X00000002, 0X00000000, 0X01000080, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0016[] = {0X00000000, 0X0000071C, 0XFFC1C000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0017[] = {0X00000000, 0X000038E0, 0XFFCE0000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0018[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0019[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0020[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0021[] = {0X01C70000, 0X00000000, 0XFFC00700, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0022[] = {0X0E380000, 0X00000000, 0XFFC03800, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0023[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0024[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0025[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0026[] = {0X00000300, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0027[] = {0X00000C00, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0028[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000619, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0029[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000039, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0030[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X000007F9, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0031[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0032[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0033[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0034[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0035[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0036[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0037[] = {0X00000001, 0X00000000, 0X00800040, 0X000007EA, };
static unsigned int _cam_data_entry_NBC_LCE_LINK_key0038[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F2, };

static IDX_MASK_ENTRY _cam_data_entry_NBC_LCE_LINK[IDX_DATA_NBC_LCE_LINK_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0000, 0, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0001, 11, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0002, 1, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0003, 0, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0004, 11, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0005, 1, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0006, 12, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0007, 12, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0008, 20, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0009, 20, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0010, 20, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0011, 20, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0012, 20, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0013, 20, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0014, 28, 10, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0015, 28, 11, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0016, 20, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0017, 28, 13, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0018, 0, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0019, 11, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0020, 1, 14, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0021, 20, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0022, 28, 16, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0023, 0, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0024, 11, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0025, 1, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0026, 20, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0027, 28, 19, 2, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0028, 0, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0029, 11, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0030, 1, 20, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0031, 0, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0032, 11, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0033, 1, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0034, 0, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0035, 11, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0036, 1, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0037, 20, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_NBC_LCE_LINK_key0038, 20, 24, 0, 0},
};

static unsigned short _cam_data_dims_NBC_LCE_LINK[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_LV,
};

static unsigned short _cam_data_expand_NBC_LCE_LINK[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_NBC_LCE_LINK =
{
    {IDX_ALGO_MASK, IDX_DATA_NBC_LCE_LINK_DIM_NS, (unsigned short*)&_cam_data_dims_NBC_LCE_LINK, (unsigned short*)&_cam_data_expand_NBC_LCE_LINK},
    {IDX_DATA_NBC_LCE_LINK_ENTRY_NS, IDX_DATA_NBC_LCE_LINK_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_NBC_LCE_LINK}
};
#define IDX_DATA_COLOR_PARAM_DIM_NS    4
#define IDX_DATA_COLOR_PARAM_FACTOR_SZ    3
#define IDX_DATA_COLOR_PARAM_ENTRY_NS    25

static unsigned int _cam_data_entry_COLOR_PARAM_key0000[] = {0X000001FC, 0X80700000, 0X00001100, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0001[] = {0X000001FC, 0X40700000, 0X00001100, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0002[] = {0X000001FC, 0X80700000, 0X00002100, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0003[] = {0X000001FC, 0X40700000, 0X00002100, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0004[] = {0X00000001, 0X40040000, 0X00001F02, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0005[] = {0X00000001, 0X80040000, 0X00001100, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0006[] = {0X00000001, 0X00040000, 0X00001F01, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0007[] = {0X00000001, 0X40040000, 0X00002F00, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0008[] = {0X00000001, 0X80040000, 0X00002100, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0009[] = {0X00000001, 0X00040000, 0X00002F01, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0010[] = {0X00000002, 0X40080000, 0X00003F02, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0011[] = {0X00000002, 0X00080000, 0X00003F01, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0012[] = {0X00000000, 0XC0000330, 0X00003FFF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0013[] = {0X00000000, 0XC0000CC0, 0X00003FFF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0014[] = {0X00000000, 0XC003F000, 0X000031FF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0015[] = {0X00CC0000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0016[] = {0X03300000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0017[] = {0XFC000000, 0XC000000F, 0X000031FF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0018[] = {0X00000600, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0019[] = {0X00001800, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0020[] = {0X0001E000, 0XC0000000, 0X000031FF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0021[] = {0X0001E1FC, 0XC0700000, 0X000036FF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0022[] = {0XFC000000, 0XC003F00F, 0X000036FF, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0023[] = {0X00000001, 0X80040000, 0X00001600, };
static unsigned int _cam_data_entry_COLOR_PARAM_key0024[] = {0X00000001, 0X80040000, 0X00002600, };

static IDX_MASK_ENTRY _cam_data_entry_COLOR_PARAM[IDX_DATA_COLOR_PARAM_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0001, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0002, 1, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0003, 1, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0004, 0, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0005, 0, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0006, 0, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0007, 1, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0008, 1, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0009, 1, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0010, 0, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0011, 0, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0012, 2, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0013, 2, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0014, 2, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0015, 3, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0016, 3, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0017, 3, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0018, 4, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0019, 4, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0020, 4, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0021, 5, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0022, 6, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0023, 0, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_COLOR_PARAM_key0024, 1, 24, 0, 0},
};

static unsigned short _cam_data_dims_COLOR_PARAM[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
};

static unsigned short _cam_data_expand_COLOR_PARAM[] = 
{0, 0, 0};

const IDX_MASK_T cam_data_COLOR_PARAM =
{
    {IDX_ALGO_MASK, IDX_DATA_COLOR_PARAM_DIM_NS, (unsigned short*)&_cam_data_dims_COLOR_PARAM, (unsigned short*)&_cam_data_expand_COLOR_PARAM},
    {IDX_DATA_COLOR_PARAM_ENTRY_NS, IDX_DATA_COLOR_PARAM_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_COLOR_PARAM}
};
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2019. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

.SLK = {
    ov13855main2mipiraw_SLK_0000[0],ov13855main2mipiraw_SLK_0000[1],ov13855main2mipiraw_SLK_0000[2],ov13855main2mipiraw_SLK_0000[3],ov13855main2mipiraw_SLK_0000[4],ov13855main2mipiraw_SLK_0000[5],ov13855main2mipiraw_SLK_0000[6],ov13855main2mipiraw_SLK_0000[7],ov13855main2mipiraw_SLK_0000[8],ov13855main2mipiraw_SLK_0000[9],
},
.DM = {
    ov13855main2mipiraw_DM_0000[0],ov13855main2mipiraw_DM_0000[1],ov13855main2mipiraw_DM_0000[2],ov13855main2mipiraw_DM_0000[3],ov13855main2mipiraw_DM_0000[4],ov13855main2mipiraw_DM_0000[5],ov13855main2mipiraw_DM_0000[6],ov13855main2mipiraw_DM_0000[7],ov13855main2mipiraw_DM_0008[0],ov13855main2mipiraw_DM_0008[1],
    ov13855main2mipiraw_DM_0008[2],ov13855main2mipiraw_DM_0008[3],ov13855main2mipiraw_DM_0008[4],ov13855main2mipiraw_DM_0008[5],ov13855main2mipiraw_DM_0008[6],ov13855main2mipiraw_DM_0008[7],ov13855main2mipiraw_DM_0016[0],ov13855main2mipiraw_DM_0016[1],ov13855main2mipiraw_DM_0016[2],ov13855main2mipiraw_DM_0016[3],
    ov13855main2mipiraw_DM_0016[4],ov13855main2mipiraw_DM_0016[5],ov13855main2mipiraw_DM_0016[6],ov13855main2mipiraw_DM_0016[7],ov13855main2mipiraw_DM_0016[8],ov13855main2mipiraw_DM_0016[9],ov13855main2mipiraw_DM_0016[10],ov13855main2mipiraw_DM_0016[11],ov13855main2mipiraw_DM_0016[12],ov13855main2mipiraw_DM_0016[13],
    ov13855main2mipiraw_DM_0016[14],ov13855main2mipiraw_DM_0016[15],ov13855main2mipiraw_DM_0032[0],ov13855main2mipiraw_DM_0032[1],ov13855main2mipiraw_DM_0032[2],ov13855main2mipiraw_DM_0032[3],ov13855main2mipiraw_DM_0032[4],ov13855main2mipiraw_DM_0032[5],ov13855main2mipiraw_DM_0032[6],ov13855main2mipiraw_DM_0032[7],
    ov13855main2mipiraw_DM_0040[0],ov13855main2mipiraw_DM_0040[1],ov13855main2mipiraw_DM_0040[2],ov13855main2mipiraw_DM_0040[3],ov13855main2mipiraw_DM_0040[4],ov13855main2mipiraw_DM_0040[5],ov13855main2mipiraw_DM_0040[6],ov13855main2mipiraw_DM_0040[7],ov13855main2mipiraw_DM_0048[0],ov13855main2mipiraw_DM_0048[1],
    ov13855main2mipiraw_DM_0048[2],ov13855main2mipiraw_DM_0048[3],ov13855main2mipiraw_DM_0048[4],ov13855main2mipiraw_DM_0048[5],ov13855main2mipiraw_DM_0048[6],ov13855main2mipiraw_DM_0048[7],ov13855main2mipiraw_DM_0056[0],ov13855main2mipiraw_DM_0056[1],ov13855main2mipiraw_DM_0056[2],ov13855main2mipiraw_DM_0056[3],
    ov13855main2mipiraw_DM_0056[4],ov13855main2mipiraw_DM_0056[5],ov13855main2mipiraw_DM_0056[6],ov13855main2mipiraw_DM_0056[7],ov13855main2mipiraw_DM_0064[0],ov13855main2mipiraw_DM_0064[1],ov13855main2mipiraw_DM_0064[2],ov13855main2mipiraw_DM_0064[3],ov13855main2mipiraw_DM_0064[4],ov13855main2mipiraw_DM_0064[5],
    ov13855main2mipiraw_DM_0064[6],ov13855main2mipiraw_DM_0064[7],
},
.YNR = {
    ov13855main2mipiraw_YNR_0000[0],ov13855main2mipiraw_YNR_0000[1],ov13855main2mipiraw_YNR_0000[2],ov13855main2mipiraw_YNR_0000[3],ov13855main2mipiraw_YNR_0000[4],ov13855main2mipiraw_YNR_0000[5],ov13855main2mipiraw_YNR_0000[6],ov13855main2mipiraw_YNR_0000[7],ov13855main2mipiraw_YNR_0008[0],ov13855main2mipiraw_YNR_0008[1],
    ov13855main2mipiraw_YNR_0008[2],ov13855main2mipiraw_YNR_0008[3],ov13855main2mipiraw_YNR_0008[4],ov13855main2mipiraw_YNR_0008[5],ov13855main2mipiraw_YNR_0008[6],ov13855main2mipiraw_YNR_0008[7],ov13855main2mipiraw_YNR_0016[0],ov13855main2mipiraw_YNR_0016[1],ov13855main2mipiraw_YNR_0016[2],ov13855main2mipiraw_YNR_0016[3],
    ov13855main2mipiraw_YNR_0016[4],ov13855main2mipiraw_YNR_0016[5],ov13855main2mipiraw_YNR_0016[6],ov13855main2mipiraw_YNR_0016[7],ov13855main2mipiraw_YNR_0016[8],ov13855main2mipiraw_YNR_0016[9],ov13855main2mipiraw_YNR_0016[10],ov13855main2mipiraw_YNR_0016[11],ov13855main2mipiraw_YNR_0016[12],ov13855main2mipiraw_YNR_0016[13],
    ov13855main2mipiraw_YNR_0016[14],ov13855main2mipiraw_YNR_0016[15],ov13855main2mipiraw_YNR_0016[16],ov13855main2mipiraw_YNR_0016[17],ov13855main2mipiraw_YNR_0016[18],ov13855main2mipiraw_YNR_0016[19],ov13855main2mipiraw_YNR_0016[20],ov13855main2mipiraw_YNR_0016[21],ov13855main2mipiraw_YNR_0016[22],ov13855main2mipiraw_YNR_0016[23],
    ov13855main2mipiraw_YNR_0040[0],ov13855main2mipiraw_YNR_0040[1],ov13855main2mipiraw_YNR_0040[2],ov13855main2mipiraw_YNR_0040[3],ov13855main2mipiraw_YNR_0040[4],ov13855main2mipiraw_YNR_0040[5],ov13855main2mipiraw_YNR_0040[6],ov13855main2mipiraw_YNR_0040[7],ov13855main2mipiraw_YNR_0040[8],ov13855main2mipiraw_YNR_0040[9],
    ov13855main2mipiraw_YNR_0040[10],ov13855main2mipiraw_YNR_0040[11],ov13855main2mipiraw_YNR_0040[12],ov13855main2mipiraw_YNR_0040[13],ov13855main2mipiraw_YNR_0040[14],ov13855main2mipiraw_YNR_0040[15],ov13855main2mipiraw_YNR_0056[0],ov13855main2mipiraw_YNR_0056[1],ov13855main2mipiraw_YNR_0056[2],ov13855main2mipiraw_YNR_0056[3],
    ov13855main2mipiraw_YNR_0056[4],ov13855main2mipiraw_YNR_0056[5],ov13855main2mipiraw_YNR_0056[6],ov13855main2mipiraw_YNR_0056[7],ov13855main2mipiraw_YNR_0064[0],ov13855main2mipiraw_YNR_0064[1],ov13855main2mipiraw_YNR_0064[2],ov13855main2mipiraw_YNR_0064[3],ov13855main2mipiraw_YNR_0064[4],ov13855main2mipiraw_YNR_0064[5],
    ov13855main2mipiraw_YNR_0064[6],ov13855main2mipiraw_YNR_0064[7],ov13855main2mipiraw_YNR_0072[0],ov13855main2mipiraw_YNR_0072[1],ov13855main2mipiraw_YNR_0072[2],ov13855main2mipiraw_YNR_0072[3],ov13855main2mipiraw_YNR_0072[4],ov13855main2mipiraw_YNR_0072[5],ov13855main2mipiraw_YNR_0072[6],ov13855main2mipiraw_YNR_0072[7],
    ov13855main2mipiraw_YNR_0072[8],ov13855main2mipiraw_YNR_0072[9],ov13855main2mipiraw_YNR_0072[10],ov13855main2mipiraw_YNR_0072[11],ov13855main2mipiraw_YNR_0072[12],ov13855main2mipiraw_YNR_0072[13],ov13855main2mipiraw_YNR_0072[14],ov13855main2mipiraw_YNR_0072[15],ov13855main2mipiraw_YNR_0072[16],ov13855main2mipiraw_YNR_0072[17],
    ov13855main2mipiraw_YNR_0072[18],ov13855main2mipiraw_YNR_0072[19],ov13855main2mipiraw_YNR_0072[20],ov13855main2mipiraw_YNR_0072[21],ov13855main2mipiraw_YNR_0072[22],ov13855main2mipiraw_YNR_0072[23],ov13855main2mipiraw_YNR_0096[0],ov13855main2mipiraw_YNR_0096[1],ov13855main2mipiraw_YNR_0096[2],ov13855main2mipiraw_YNR_0096[3],
    ov13855main2mipiraw_YNR_0096[4],ov13855main2mipiraw_YNR_0096[5],ov13855main2mipiraw_YNR_0096[6],ov13855main2mipiraw_YNR_0096[7],ov13855main2mipiraw_YNR_0104[0],ov13855main2mipiraw_YNR_0104[1],ov13855main2mipiraw_YNR_0104[2],ov13855main2mipiraw_YNR_0104[3],ov13855main2mipiraw_YNR_0104[4],ov13855main2mipiraw_YNR_0104[5],
    ov13855main2mipiraw_YNR_0104[6],ov13855main2mipiraw_YNR_0104[7],
},
.YNR_LCE_LINK = {
    ov13855main2mipiraw_YNR_LCE_LINK_0000[0],ov13855main2mipiraw_YNR_LCE_LINK_0000[1],ov13855main2mipiraw_YNR_LCE_LINK_0000[2],ov13855main2mipiraw_YNR_LCE_LINK_0000[3],ov13855main2mipiraw_YNR_LCE_LINK_0000[4],ov13855main2mipiraw_YNR_LCE_LINK_0000[5],ov13855main2mipiraw_YNR_LCE_LINK_0000[6],ov13855main2mipiraw_YNR_LCE_LINK_0000[7],ov13855main2mipiraw_YNR_LCE_LINK_0008[0],ov13855main2mipiraw_YNR_LCE_LINK_0008[1],
    ov13855main2mipiraw_YNR_LCE_LINK_0008[2],ov13855main2mipiraw_YNR_LCE_LINK_0008[3],ov13855main2mipiraw_YNR_LCE_LINK_0008[4],ov13855main2mipiraw_YNR_LCE_LINK_0008[5],ov13855main2mipiraw_YNR_LCE_LINK_0008[6],ov13855main2mipiraw_YNR_LCE_LINK_0008[7],ov13855main2mipiraw_YNR_LCE_LINK_0008[8],ov13855main2mipiraw_YNR_LCE_LINK_0008[9],ov13855main2mipiraw_YNR_LCE_LINK_0008[10],ov13855main2mipiraw_YNR_LCE_LINK_0008[11],
    ov13855main2mipiraw_YNR_LCE_LINK_0008[12],ov13855main2mipiraw_YNR_LCE_LINK_0008[13],ov13855main2mipiraw_YNR_LCE_LINK_0008[14],ov13855main2mipiraw_YNR_LCE_LINK_0008[15],ov13855main2mipiraw_YNR_LCE_LINK_0024[0],ov13855main2mipiraw_YNR_LCE_LINK_0024[1],ov13855main2mipiraw_YNR_LCE_LINK_0024[2],ov13855main2mipiraw_YNR_LCE_LINK_0024[3],ov13855main2mipiraw_YNR_LCE_LINK_0024[4],ov13855main2mipiraw_YNR_LCE_LINK_0024[5],
    ov13855main2mipiraw_YNR_LCE_LINK_0024[6],ov13855main2mipiraw_YNR_LCE_LINK_0024[7],ov13855main2mipiraw_YNR_LCE_LINK_0032[0],ov13855main2mipiraw_YNR_LCE_LINK_0032[1],ov13855main2mipiraw_YNR_LCE_LINK_0032[2],ov13855main2mipiraw_YNR_LCE_LINK_0032[3],ov13855main2mipiraw_YNR_LCE_LINK_0032[4],ov13855main2mipiraw_YNR_LCE_LINK_0032[5],ov13855main2mipiraw_YNR_LCE_LINK_0032[6],ov13855main2mipiraw_YNR_LCE_LINK_0032[7],
    ov13855main2mipiraw_YNR_LCE_LINK_0040[0],ov13855main2mipiraw_YNR_LCE_LINK_0040[1],ov13855main2mipiraw_YNR_LCE_LINK_0040[2],ov13855main2mipiraw_YNR_LCE_LINK_0040[3],ov13855main2mipiraw_YNR_LCE_LINK_0040[4],ov13855main2mipiraw_YNR_LCE_LINK_0040[5],ov13855main2mipiraw_YNR_LCE_LINK_0040[6],ov13855main2mipiraw_YNR_LCE_LINK_0040[7],
},
.YNR_FD = {
    ov13855main2mipiraw_YNR_FD_0000[0],ov13855main2mipiraw_YNR_FD_0000[1],ov13855main2mipiraw_YNR_FD_0000[2],ov13855main2mipiraw_YNR_FD_0000[3],ov13855main2mipiraw_YNR_FD_0000[4],ov13855main2mipiraw_YNR_FD_0000[5],ov13855main2mipiraw_YNR_FD_0000[6],ov13855main2mipiraw_YNR_FD_0000[7],ov13855main2mipiraw_YNR_FD_0008[0],ov13855main2mipiraw_YNR_FD_0008[1],
    ov13855main2mipiraw_YNR_FD_0008[2],ov13855main2mipiraw_YNR_FD_0008[3],ov13855main2mipiraw_YNR_FD_0008[4],ov13855main2mipiraw_YNR_FD_0008[5],ov13855main2mipiraw_YNR_FD_0008[6],ov13855main2mipiraw_YNR_FD_0008[7],ov13855main2mipiraw_YNR_FD_0008[8],ov13855main2mipiraw_YNR_FD_0008[9],ov13855main2mipiraw_YNR_FD_0008[10],ov13855main2mipiraw_YNR_FD_0008[11],
    ov13855main2mipiraw_YNR_FD_0008[12],ov13855main2mipiraw_YNR_FD_0008[13],ov13855main2mipiraw_YNR_FD_0008[14],ov13855main2mipiraw_YNR_FD_0008[15],ov13855main2mipiraw_YNR_FD_0024[0],ov13855main2mipiraw_YNR_FD_0024[1],ov13855main2mipiraw_YNR_FD_0024[2],ov13855main2mipiraw_YNR_FD_0024[3],ov13855main2mipiraw_YNR_FD_0024[4],ov13855main2mipiraw_YNR_FD_0024[5],
    ov13855main2mipiraw_YNR_FD_0024[6],ov13855main2mipiraw_YNR_FD_0024[7],ov13855main2mipiraw_YNR_FD_0032[0],ov13855main2mipiraw_YNR_FD_0032[1],ov13855main2mipiraw_YNR_FD_0032[2],ov13855main2mipiraw_YNR_FD_0032[3],ov13855main2mipiraw_YNR_FD_0032[4],ov13855main2mipiraw_YNR_FD_0032[5],ov13855main2mipiraw_YNR_FD_0032[6],ov13855main2mipiraw_YNR_FD_0032[7],
    ov13855main2mipiraw_YNR_FD_0040[0],ov13855main2mipiraw_YNR_FD_0040[1],ov13855main2mipiraw_YNR_FD_0040[2],ov13855main2mipiraw_YNR_FD_0040[3],ov13855main2mipiraw_YNR_FD_0040[4],ov13855main2mipiraw_YNR_FD_0040[5],ov13855main2mipiraw_YNR_FD_0040[6],ov13855main2mipiraw_YNR_FD_0040[7],
},
.CNR_CNR = {
    ov13855main2mipiraw_CNR_CNR_0000[0],ov13855main2mipiraw_CNR_CNR_0000[1],ov13855main2mipiraw_CNR_CNR_0000[2],ov13855main2mipiraw_CNR_CNR_0000[3],ov13855main2mipiraw_CNR_CNR_0000[4],ov13855main2mipiraw_CNR_CNR_0000[5],ov13855main2mipiraw_CNR_CNR_0000[6],ov13855main2mipiraw_CNR_CNR_0000[7],ov13855main2mipiraw_CNR_CNR_0008[0],ov13855main2mipiraw_CNR_CNR_0008[1],
    ov13855main2mipiraw_CNR_CNR_0008[2],ov13855main2mipiraw_CNR_CNR_0008[3],ov13855main2mipiraw_CNR_CNR_0008[4],ov13855main2mipiraw_CNR_CNR_0008[5],ov13855main2mipiraw_CNR_CNR_0008[6],ov13855main2mipiraw_CNR_CNR_0008[7],ov13855main2mipiraw_CNR_CNR_0008[8],ov13855main2mipiraw_CNR_CNR_0008[9],ov13855main2mipiraw_CNR_CNR_0008[10],ov13855main2mipiraw_CNR_CNR_0008[11],
    ov13855main2mipiraw_CNR_CNR_0008[12],ov13855main2mipiraw_CNR_CNR_0008[13],ov13855main2mipiraw_CNR_CNR_0008[14],ov13855main2mipiraw_CNR_CNR_0008[15],ov13855main2mipiraw_CNR_CNR_0008[16],ov13855main2mipiraw_CNR_CNR_0008[17],ov13855main2mipiraw_CNR_CNR_0008[18],ov13855main2mipiraw_CNR_CNR_0008[19],ov13855main2mipiraw_CNR_CNR_0008[20],ov13855main2mipiraw_CNR_CNR_0008[21],
    ov13855main2mipiraw_CNR_CNR_0008[22],ov13855main2mipiraw_CNR_CNR_0008[23],ov13855main2mipiraw_CNR_CNR_0032[0],ov13855main2mipiraw_CNR_CNR_0032[1],ov13855main2mipiraw_CNR_CNR_0032[2],ov13855main2mipiraw_CNR_CNR_0032[3],ov13855main2mipiraw_CNR_CNR_0032[4],ov13855main2mipiraw_CNR_CNR_0032[5],ov13855main2mipiraw_CNR_CNR_0032[6],ov13855main2mipiraw_CNR_CNR_0032[7],
    ov13855main2mipiraw_CNR_CNR_0032[8],ov13855main2mipiraw_CNR_CNR_0032[9],ov13855main2mipiraw_CNR_CNR_0032[10],ov13855main2mipiraw_CNR_CNR_0032[11],ov13855main2mipiraw_CNR_CNR_0032[12],ov13855main2mipiraw_CNR_CNR_0032[13],ov13855main2mipiraw_CNR_CNR_0032[14],ov13855main2mipiraw_CNR_CNR_0032[15],ov13855main2mipiraw_CNR_CNR_0048[0],ov13855main2mipiraw_CNR_CNR_0048[1],
    ov13855main2mipiraw_CNR_CNR_0048[2],ov13855main2mipiraw_CNR_CNR_0048[3],ov13855main2mipiraw_CNR_CNR_0048[4],ov13855main2mipiraw_CNR_CNR_0048[5],ov13855main2mipiraw_CNR_CNR_0048[6],ov13855main2mipiraw_CNR_CNR_0048[7],ov13855main2mipiraw_CNR_CNR_0056[0],ov13855main2mipiraw_CNR_CNR_0056[1],ov13855main2mipiraw_CNR_CNR_0056[2],ov13855main2mipiraw_CNR_CNR_0056[3],
    ov13855main2mipiraw_CNR_CNR_0056[4],ov13855main2mipiraw_CNR_CNR_0056[5],ov13855main2mipiraw_CNR_CNR_0056[6],ov13855main2mipiraw_CNR_CNR_0056[7],ov13855main2mipiraw_CNR_CNR_0056[8],ov13855main2mipiraw_CNR_CNR_0056[9],ov13855main2mipiraw_CNR_CNR_0056[10],ov13855main2mipiraw_CNR_CNR_0056[11],ov13855main2mipiraw_CNR_CNR_0056[12],ov13855main2mipiraw_CNR_CNR_0056[13],
    ov13855main2mipiraw_CNR_CNR_0056[14],ov13855main2mipiraw_CNR_CNR_0056[15],ov13855main2mipiraw_CNR_CNR_0056[16],ov13855main2mipiraw_CNR_CNR_0056[17],ov13855main2mipiraw_CNR_CNR_0056[18],ov13855main2mipiraw_CNR_CNR_0056[19],ov13855main2mipiraw_CNR_CNR_0056[20],ov13855main2mipiraw_CNR_CNR_0056[21],ov13855main2mipiraw_CNR_CNR_0056[22],ov13855main2mipiraw_CNR_CNR_0056[23],
    ov13855main2mipiraw_CNR_CNR_0080[0],ov13855main2mipiraw_CNR_CNR_0080[1],ov13855main2mipiraw_CNR_CNR_0080[2],ov13855main2mipiraw_CNR_CNR_0080[3],ov13855main2mipiraw_CNR_CNR_0080[4],ov13855main2mipiraw_CNR_CNR_0080[5],ov13855main2mipiraw_CNR_CNR_0080[6],ov13855main2mipiraw_CNR_CNR_0080[7],ov13855main2mipiraw_CNR_CNR_0088[0],ov13855main2mipiraw_CNR_CNR_0088[1],
    ov13855main2mipiraw_CNR_CNR_0088[2],ov13855main2mipiraw_CNR_CNR_0088[3],ov13855main2mipiraw_CNR_CNR_0088[4],ov13855main2mipiraw_CNR_CNR_0088[5],ov13855main2mipiraw_CNR_CNR_0088[6],ov13855main2mipiraw_CNR_CNR_0088[7],
},
.EE = {
    ov13855main2mipiraw_EE_0000[0],ov13855main2mipiraw_EE_0000[1],ov13855main2mipiraw_EE_0000[2],ov13855main2mipiraw_EE_0000[3],ov13855main2mipiraw_EE_0000[4],ov13855main2mipiraw_EE_0000[5],ov13855main2mipiraw_EE_0000[6],ov13855main2mipiraw_EE_0000[7],ov13855main2mipiraw_EE_0008[0],ov13855main2mipiraw_EE_0008[1],
    ov13855main2mipiraw_EE_0008[2],ov13855main2mipiraw_EE_0008[3],ov13855main2mipiraw_EE_0008[4],ov13855main2mipiraw_EE_0008[5],ov13855main2mipiraw_EE_0008[6],ov13855main2mipiraw_EE_0008[7],ov13855main2mipiraw_EE_0016[0],ov13855main2mipiraw_EE_0016[1],ov13855main2mipiraw_EE_0016[2],ov13855main2mipiraw_EE_0016[3],
    ov13855main2mipiraw_EE_0016[4],ov13855main2mipiraw_EE_0016[5],ov13855main2mipiraw_EE_0016[6],ov13855main2mipiraw_EE_0016[7],ov13855main2mipiraw_EE_0024[0],ov13855main2mipiraw_EE_0024[1],ov13855main2mipiraw_EE_0024[2],ov13855main2mipiraw_EE_0024[3],ov13855main2mipiraw_EE_0024[4],ov13855main2mipiraw_EE_0024[5],
    ov13855main2mipiraw_EE_0024[6],ov13855main2mipiraw_EE_0024[7],ov13855main2mipiraw_EE_0024[8],ov13855main2mipiraw_EE_0024[9],ov13855main2mipiraw_EE_0024[10],ov13855main2mipiraw_EE_0024[11],ov13855main2mipiraw_EE_0024[12],ov13855main2mipiraw_EE_0024[13],ov13855main2mipiraw_EE_0024[14],ov13855main2mipiraw_EE_0024[15],
    ov13855main2mipiraw_EE_0040[0],ov13855main2mipiraw_EE_0040[1],ov13855main2mipiraw_EE_0040[2],ov13855main2mipiraw_EE_0040[3],ov13855main2mipiraw_EE_0040[4],ov13855main2mipiraw_EE_0040[5],ov13855main2mipiraw_EE_0040[6],ov13855main2mipiraw_EE_0040[7],ov13855main2mipiraw_EE_0048[0],ov13855main2mipiraw_EE_0048[1],
    ov13855main2mipiraw_EE_0048[2],ov13855main2mipiraw_EE_0048[3],ov13855main2mipiraw_EE_0048[4],ov13855main2mipiraw_EE_0048[5],ov13855main2mipiraw_EE_0048[6],ov13855main2mipiraw_EE_0048[7],ov13855main2mipiraw_EE_0056[0],ov13855main2mipiraw_EE_0056[1],ov13855main2mipiraw_EE_0056[2],ov13855main2mipiraw_EE_0056[3],
    ov13855main2mipiraw_EE_0056[4],ov13855main2mipiraw_EE_0056[5],ov13855main2mipiraw_EE_0056[6],ov13855main2mipiraw_EE_0056[7],ov13855main2mipiraw_EE_0064[0],ov13855main2mipiraw_EE_0064[1],ov13855main2mipiraw_EE_0064[2],ov13855main2mipiraw_EE_0064[3],ov13855main2mipiraw_EE_0064[4],ov13855main2mipiraw_EE_0064[5],
    ov13855main2mipiraw_EE_0064[6],ov13855main2mipiraw_EE_0064[7],ov13855main2mipiraw_EE_0072[0],ov13855main2mipiraw_EE_0072[1],ov13855main2mipiraw_EE_0072[2],ov13855main2mipiraw_EE_0072[3],ov13855main2mipiraw_EE_0072[4],ov13855main2mipiraw_EE_0072[5],ov13855main2mipiraw_EE_0072[6],ov13855main2mipiraw_EE_0072[7],
},
.NR3D = {
    ov13855main2mipiraw_NR3D_0000[0],ov13855main2mipiraw_NR3D_0000[1],ov13855main2mipiraw_NR3D_0000[2],ov13855main2mipiraw_NR3D_0000[3],ov13855main2mipiraw_NR3D_0000[4],ov13855main2mipiraw_NR3D_0000[5],ov13855main2mipiraw_NR3D_0000[6],ov13855main2mipiraw_NR3D_0000[7],ov13855main2mipiraw_NR3D_0008[0],ov13855main2mipiraw_NR3D_0008[1],
    ov13855main2mipiraw_NR3D_0008[2],ov13855main2mipiraw_NR3D_0008[3],ov13855main2mipiraw_NR3D_0008[4],ov13855main2mipiraw_NR3D_0008[5],ov13855main2mipiraw_NR3D_0008[6],ov13855main2mipiraw_NR3D_0008[7],ov13855main2mipiraw_NR3D_0016[0],ov13855main2mipiraw_NR3D_0016[1],ov13855main2mipiraw_NR3D_0016[2],ov13855main2mipiraw_NR3D_0016[3],
    ov13855main2mipiraw_NR3D_0016[4],ov13855main2mipiraw_NR3D_0016[5],ov13855main2mipiraw_NR3D_0016[6],ov13855main2mipiraw_NR3D_0016[7],ov13855main2mipiraw_NR3D_0024[0],ov13855main2mipiraw_NR3D_0024[1],ov13855main2mipiraw_NR3D_0024[2],ov13855main2mipiraw_NR3D_0024[3],ov13855main2mipiraw_NR3D_0024[4],ov13855main2mipiraw_NR3D_0024[5],
    ov13855main2mipiraw_NR3D_0024[6],ov13855main2mipiraw_NR3D_0024[7],
},
.OBC = {
    ov13855main2mipiraw_OBC_0000[0],ov13855main2mipiraw_OBC_0000[1],ov13855main2mipiraw_OBC_0000[2],ov13855main2mipiraw_OBC_0000[3],ov13855main2mipiraw_OBC_0000[4],ov13855main2mipiraw_OBC_0000[5],ov13855main2mipiraw_OBC_0000[6],ov13855main2mipiraw_OBC_0000[7],ov13855main2mipiraw_OBC_0008[0],ov13855main2mipiraw_OBC_0008[1],
    ov13855main2mipiraw_OBC_0008[2],ov13855main2mipiraw_OBC_0008[3],ov13855main2mipiraw_OBC_0008[4],ov13855main2mipiraw_OBC_0008[5],ov13855main2mipiraw_OBC_0008[6],ov13855main2mipiraw_OBC_0008[7],ov13855main2mipiraw_OBC_0016[0],ov13855main2mipiraw_OBC_0016[1],ov13855main2mipiraw_OBC_0016[2],ov13855main2mipiraw_OBC_0016[3],
    ov13855main2mipiraw_OBC_0016[4],ov13855main2mipiraw_OBC_0016[5],ov13855main2mipiraw_OBC_0016[6],ov13855main2mipiraw_OBC_0016[7],
},
.BPC_BPC = {
    ov13855main2mipiraw_BPC_BPC_0000[0],ov13855main2mipiraw_BPC_BPC_0000[1],ov13855main2mipiraw_BPC_BPC_0000[2],ov13855main2mipiraw_BPC_BPC_0000[3],ov13855main2mipiraw_BPC_BPC_0000[4],ov13855main2mipiraw_BPC_BPC_0000[5],ov13855main2mipiraw_BPC_BPC_0000[6],ov13855main2mipiraw_BPC_BPC_0000[7],ov13855main2mipiraw_BPC_BPC_0008[0],ov13855main2mipiraw_BPC_BPC_0008[1],
    ov13855main2mipiraw_BPC_BPC_0008[2],ov13855main2mipiraw_BPC_BPC_0008[3],ov13855main2mipiraw_BPC_BPC_0008[4],ov13855main2mipiraw_BPC_BPC_0008[5],ov13855main2mipiraw_BPC_BPC_0008[6],ov13855main2mipiraw_BPC_BPC_0008[7],ov13855main2mipiraw_BPC_BPC_0016[0],ov13855main2mipiraw_BPC_BPC_0016[1],ov13855main2mipiraw_BPC_BPC_0016[2],ov13855main2mipiraw_BPC_BPC_0016[3],
    ov13855main2mipiraw_BPC_BPC_0016[4],ov13855main2mipiraw_BPC_BPC_0016[5],ov13855main2mipiraw_BPC_BPC_0016[6],ov13855main2mipiraw_BPC_BPC_0016[7],
},
.BPC_CT = {
    ov13855main2mipiraw_BPC_CT_0000[0],ov13855main2mipiraw_BPC_CT_0000[1],ov13855main2mipiraw_BPC_CT_0000[2],ov13855main2mipiraw_BPC_CT_0000[3],ov13855main2mipiraw_BPC_CT_0000[4],ov13855main2mipiraw_BPC_CT_0000[5],ov13855main2mipiraw_BPC_CT_0000[6],ov13855main2mipiraw_BPC_CT_0000[7],ov13855main2mipiraw_BPC_CT_0008[0],ov13855main2mipiraw_BPC_CT_0008[1],
    ov13855main2mipiraw_BPC_CT_0008[2],ov13855main2mipiraw_BPC_CT_0008[3],ov13855main2mipiraw_BPC_CT_0008[4],ov13855main2mipiraw_BPC_CT_0008[5],ov13855main2mipiraw_BPC_CT_0008[6],ov13855main2mipiraw_BPC_CT_0008[7],ov13855main2mipiraw_BPC_CT_0016[0],ov13855main2mipiraw_BPC_CT_0016[1],ov13855main2mipiraw_BPC_CT_0016[2],ov13855main2mipiraw_BPC_CT_0016[3],
    ov13855main2mipiraw_BPC_CT_0016[4],ov13855main2mipiraw_BPC_CT_0016[5],ov13855main2mipiraw_BPC_CT_0016[6],ov13855main2mipiraw_BPC_CT_0016[7],
},
.BPC_PDC = {
    ov13855main2mipiraw_BPC_PDC_0000[0],ov13855main2mipiraw_BPC_PDC_0000[1],ov13855main2mipiraw_BPC_PDC_0000[2],ov13855main2mipiraw_BPC_PDC_0000[3],ov13855main2mipiraw_BPC_PDC_0000[4],ov13855main2mipiraw_BPC_PDC_0000[5],ov13855main2mipiraw_BPC_PDC_0000[6],ov13855main2mipiraw_BPC_PDC_0000[7],ov13855main2mipiraw_BPC_PDC_0008[0],ov13855main2mipiraw_BPC_PDC_0008[1],
    ov13855main2mipiraw_BPC_PDC_0008[2],ov13855main2mipiraw_BPC_PDC_0008[3],ov13855main2mipiraw_BPC_PDC_0008[4],ov13855main2mipiraw_BPC_PDC_0008[5],ov13855main2mipiraw_BPC_PDC_0008[6],ov13855main2mipiraw_BPC_PDC_0008[7],ov13855main2mipiraw_BPC_PDC_0016[0],ov13855main2mipiraw_BPC_PDC_0016[1],ov13855main2mipiraw_BPC_PDC_0016[2],ov13855main2mipiraw_BPC_PDC_0016[3],
    ov13855main2mipiraw_BPC_PDC_0016[4],ov13855main2mipiraw_BPC_PDC_0016[5],ov13855main2mipiraw_BPC_PDC_0016[6],ov13855main2mipiraw_BPC_PDC_0016[7],
},
.LDNR = {
    ov13855main2mipiraw_LDNR_0000[0],ov13855main2mipiraw_LDNR_0000[1],ov13855main2mipiraw_LDNR_0000[2],ov13855main2mipiraw_LDNR_0000[3],ov13855main2mipiraw_LDNR_0000[4],ov13855main2mipiraw_LDNR_0000[5],ov13855main2mipiraw_LDNR_0000[6],ov13855main2mipiraw_LDNR_0000[7],ov13855main2mipiraw_LDNR_0008[0],ov13855main2mipiraw_LDNR_0008[1],
    ov13855main2mipiraw_LDNR_0008[2],ov13855main2mipiraw_LDNR_0008[3],ov13855main2mipiraw_LDNR_0008[4],ov13855main2mipiraw_LDNR_0008[5],ov13855main2mipiraw_LDNR_0008[6],ov13855main2mipiraw_LDNR_0008[7],ov13855main2mipiraw_LDNR_0016[0],ov13855main2mipiraw_LDNR_0016[1],ov13855main2mipiraw_LDNR_0016[2],ov13855main2mipiraw_LDNR_0016[3],
    ov13855main2mipiraw_LDNR_0016[4],ov13855main2mipiraw_LDNR_0016[5],ov13855main2mipiraw_LDNR_0016[6],ov13855main2mipiraw_LDNR_0016[7],
},
.CNR_CCR = {
    ov13855main2mipiraw_CNR_CCR_0000[0],ov13855main2mipiraw_CNR_CCR_0000[1],ov13855main2mipiraw_CNR_CCR_0000[2],ov13855main2mipiraw_CNR_CCR_0000[3],ov13855main2mipiraw_CNR_CCR_0000[4],ov13855main2mipiraw_CNR_CCR_0000[5],ov13855main2mipiraw_CNR_CCR_0000[6],ov13855main2mipiraw_CNR_CCR_0000[7],
},
.CNR_ABF = {
    ov13855main2mipiraw_CNR_ABF_0000[0],ov13855main2mipiraw_CNR_ABF_0000[1],ov13855main2mipiraw_CNR_ABF_0000[2],ov13855main2mipiraw_CNR_ABF_0000[3],ov13855main2mipiraw_CNR_ABF_0000[4],ov13855main2mipiraw_CNR_ABF_0000[5],ov13855main2mipiraw_CNR_ABF_0000[6],ov13855main2mipiraw_CNR_ABF_0000[7],
},
.LPCNR = {
    ov13855main2mipiraw_LPCNR_0000[0],ov13855main2mipiraw_LPCNR_0000[1],ov13855main2mipiraw_LPCNR_0000[2],ov13855main2mipiraw_LPCNR_0000[3],ov13855main2mipiraw_LPCNR_0000[4],ov13855main2mipiraw_LPCNR_0000[5],ov13855main2mipiraw_LPCNR_0000[6],ov13855main2mipiraw_LPCNR_0000[7],ov13855main2mipiraw_LPCNR_0008[0],ov13855main2mipiraw_LPCNR_0008[1],
    ov13855main2mipiraw_LPCNR_0008[2],ov13855main2mipiraw_LPCNR_0008[3],ov13855main2mipiraw_LPCNR_0008[4],ov13855main2mipiraw_LPCNR_0008[5],ov13855main2mipiraw_LPCNR_0008[6],ov13855main2mipiraw_LPCNR_0008[7],ov13855main2mipiraw_LPCNR_0016[0],ov13855main2mipiraw_LPCNR_0016[1],ov13855main2mipiraw_LPCNR_0016[2],ov13855main2mipiraw_LPCNR_0016[3],
    ov13855main2mipiraw_LPCNR_0016[4],ov13855main2mipiraw_LPCNR_0016[5],ov13855main2mipiraw_LPCNR_0016[6],ov13855main2mipiraw_LPCNR_0016[7],ov13855main2mipiraw_LPCNR_0024[0],ov13855main2mipiraw_LPCNR_0024[1],ov13855main2mipiraw_LPCNR_0024[2],ov13855main2mipiraw_LPCNR_0024[3],ov13855main2mipiraw_LPCNR_0024[4],ov13855main2mipiraw_LPCNR_0024[5],
    ov13855main2mipiraw_LPCNR_0024[6],ov13855main2mipiraw_LPCNR_0024[7],
},
.FUS = {
    ov13855main2mipiraw_FUS_0000[0],ov13855main2mipiraw_FUS_0000[1],ov13855main2mipiraw_FUS_0000[2],ov13855main2mipiraw_FUS_0000[3],ov13855main2mipiraw_FUS_0000[4],ov13855main2mipiraw_FUS_0000[5],ov13855main2mipiraw_FUS_0000[6],ov13855main2mipiraw_FUS_0000[7],
},
.ZFUS = {
    ov13855main2mipiraw_ZFUS_0000[0],ov13855main2mipiraw_ZFUS_0000[1],ov13855main2mipiraw_ZFUS_0000[2],ov13855main2mipiraw_ZFUS_0000[3],ov13855main2mipiraw_ZFUS_0000[4],ov13855main2mipiraw_ZFUS_0000[5],ov13855main2mipiraw_ZFUS_0000[6],ov13855main2mipiraw_ZFUS_0000[7],
},
.MFB = {
    ov13855main2mipiraw_MFB_0000[0],ov13855main2mipiraw_MFB_0000[1],ov13855main2mipiraw_MFB_0000[2],ov13855main2mipiraw_MFB_0000[3],ov13855main2mipiraw_MFB_0000[4],ov13855main2mipiraw_MFB_0000[5],ov13855main2mipiraw_MFB_0000[6],ov13855main2mipiraw_MFB_0000[7],ov13855main2mipiraw_MFB_0008[0],ov13855main2mipiraw_MFB_0008[1],
    ov13855main2mipiraw_MFB_0008[2],ov13855main2mipiraw_MFB_0008[3],ov13855main2mipiraw_MFB_0008[4],ov13855main2mipiraw_MFB_0008[5],ov13855main2mipiraw_MFB_0008[6],ov13855main2mipiraw_MFB_0008[7],
},
.MIX = {
    ov13855main2mipiraw_MIX_0000[0],ov13855main2mipiraw_MIX_0000[1],ov13855main2mipiraw_MIX_0000[2],ov13855main2mipiraw_MIX_0000[3],ov13855main2mipiraw_MIX_0000[4],ov13855main2mipiraw_MIX_0000[5],ov13855main2mipiraw_MIX_0000[6],ov13855main2mipiraw_MIX_0000[7],ov13855main2mipiraw_MIX_0008[0],ov13855main2mipiraw_MIX_0008[1],
    ov13855main2mipiraw_MIX_0008[2],ov13855main2mipiraw_MIX_0008[3],ov13855main2mipiraw_MIX_0008[4],ov13855main2mipiraw_MIX_0008[5],ov13855main2mipiraw_MIX_0008[6],ov13855main2mipiraw_MIX_0008[7],
},
.YNRS = {
    ov13855main2mipiraw_YNRS_0000[0],ov13855main2mipiraw_YNRS_0000[1],ov13855main2mipiraw_YNRS_0000[2],ov13855main2mipiraw_YNRS_0000[3],ov13855main2mipiraw_YNRS_0000[4],ov13855main2mipiraw_YNRS_0000[5],ov13855main2mipiraw_YNRS_0000[6],ov13855main2mipiraw_YNRS_0000[7],
},
.YNR_TBL = {
    ov13855main2mipiraw_YNR_TBL_0000[0],ov13855main2mipiraw_YNR_TBL_0000[1],ov13855main2mipiraw_YNR_TBL_0000[2],ov13855main2mipiraw_YNR_TBL_0000[3],ov13855main2mipiraw_YNR_TBL_0000[4],ov13855main2mipiraw_YNR_TBL_0000[5],ov13855main2mipiraw_YNR_TBL_0000[6],ov13855main2mipiraw_YNR_TBL_0000[7],ov13855main2mipiraw_YNR_TBL_0008[0],ov13855main2mipiraw_YNR_TBL_0008[1],
    ov13855main2mipiraw_YNR_TBL_0008[2],ov13855main2mipiraw_YNR_TBL_0008[3],ov13855main2mipiraw_YNR_TBL_0008[4],ov13855main2mipiraw_YNR_TBL_0008[5],ov13855main2mipiraw_YNR_TBL_0008[6],ov13855main2mipiraw_YNR_TBL_0008[7],
},

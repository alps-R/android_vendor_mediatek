const NVRAM_CAMERA_FEATURE_AINR_THRES_STRUCT AINR_THRES_%04d = {
    // Gives N, for capturing N frames
    .capture_frame_number=0,

    .ainr_iso_th =0,

    // iso range for adaptive frame number mechanism 
    .iso_level1 =0, 
    .iso_level2 =0,
    .iso_level3 =0,
    .iso_level4 =0,
    .iso_level5 =0,
    .iso_level6 =0,
    .iso_level7 =0,
    .iso_level8 =0,

    // blend frame number
    .frame_num1 =0, 
    .frame_num2 =0,
    .frame_num3 =0,
    .frame_num4 =0,
    .frame_num5 =0,
    .frame_num6 =0,
    .frame_num7 =0,
    .frame_num8 =0
};

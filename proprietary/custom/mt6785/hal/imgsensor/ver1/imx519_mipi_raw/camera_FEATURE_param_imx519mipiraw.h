/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

.MFNR = {
    imx519mipiraw_MFNR_0000, imx519mipiraw_MFNR_0001, imx519mipiraw_MFNR_0002, imx519mipiraw_MFNR_0003, imx519mipiraw_MFNR_0004, imx519mipiraw_MFNR_0005, imx519mipiraw_MFNR_0006, imx519mipiraw_MFNR_0007, imx519mipiraw_MFNR_0008, imx519mipiraw_MFNR_0009,
},
.SWNR = {
    imx519mipiraw_SWNR_0000, imx519mipiraw_SWNR_0001, imx519mipiraw_SWNR_0002, imx519mipiraw_SWNR_0003, imx519mipiraw_SWNR_0004, imx519mipiraw_SWNR_0005, imx519mipiraw_SWNR_0006, imx519mipiraw_SWNR_0007, imx519mipiraw_SWNR_0008, imx519mipiraw_SWNR_0009,
    imx519mipiraw_SWNR_0010, imx519mipiraw_SWNR_0011, imx519mipiraw_SWNR_0012, imx519mipiraw_SWNR_0013, imx519mipiraw_SWNR_0014, imx519mipiraw_SWNR_0015, imx519mipiraw_SWNR_0016, imx519mipiraw_SWNR_0017, imx519mipiraw_SWNR_0018, imx519mipiraw_SWNR_0019,
    imx519mipiraw_SWNR_0020, imx519mipiraw_SWNR_0021, imx519mipiraw_SWNR_0022, imx519mipiraw_SWNR_0023, imx519mipiraw_SWNR_0024, imx519mipiraw_SWNR_0025, imx519mipiraw_SWNR_0026, imx519mipiraw_SWNR_0027, imx519mipiraw_SWNR_0028, imx519mipiraw_SWNR_0029,
    imx519mipiraw_SWNR_0030, imx519mipiraw_SWNR_0031, imx519mipiraw_SWNR_0032, imx519mipiraw_SWNR_0033, imx519mipiraw_SWNR_0034, imx519mipiraw_SWNR_0035, imx519mipiraw_SWNR_0036, imx519mipiraw_SWNR_0037, imx519mipiraw_SWNR_0038, imx519mipiraw_SWNR_0039,
    imx519mipiraw_SWNR_0040, imx519mipiraw_SWNR_0041, imx519mipiraw_SWNR_0042, imx519mipiraw_SWNR_0043, imx519mipiraw_SWNR_0044, imx519mipiraw_SWNR_0045, imx519mipiraw_SWNR_0046, imx519mipiraw_SWNR_0047, imx519mipiraw_SWNR_0048, imx519mipiraw_SWNR_0049,
    imx519mipiraw_SWNR_0050, imx519mipiraw_SWNR_0051, imx519mipiraw_SWNR_0052, imx519mipiraw_SWNR_0053, imx519mipiraw_SWNR_0054, imx519mipiraw_SWNR_0055, imx519mipiraw_SWNR_0056, imx519mipiraw_SWNR_0057, imx519mipiraw_SWNR_0058, imx519mipiraw_SWNR_0059,
    imx519mipiraw_SWNR_0060, imx519mipiraw_SWNR_0061, imx519mipiraw_SWNR_0062, imx519mipiraw_SWNR_0063, imx519mipiraw_SWNR_0064, imx519mipiraw_SWNR_0065, imx519mipiraw_SWNR_0066, imx519mipiraw_SWNR_0067, imx519mipiraw_SWNR_0068, imx519mipiraw_SWNR_0069,
    imx519mipiraw_SWNR_0070, imx519mipiraw_SWNR_0071, imx519mipiraw_SWNR_0072, imx519mipiraw_SWNR_0073, imx519mipiraw_SWNR_0074, imx519mipiraw_SWNR_0075, imx519mipiraw_SWNR_0076, imx519mipiraw_SWNR_0077, imx519mipiraw_SWNR_0078, imx519mipiraw_SWNR_0079,
    imx519mipiraw_SWNR_0080, imx519mipiraw_SWNR_0081, imx519mipiraw_SWNR_0082, imx519mipiraw_SWNR_0083, imx519mipiraw_SWNR_0084, imx519mipiraw_SWNR_0085, imx519mipiraw_SWNR_0086, imx519mipiraw_SWNR_0087, imx519mipiraw_SWNR_0088, imx519mipiraw_SWNR_0089,
    imx519mipiraw_SWNR_0090, imx519mipiraw_SWNR_0091, imx519mipiraw_SWNR_0092, imx519mipiraw_SWNR_0093, imx519mipiraw_SWNR_0094, imx519mipiraw_SWNR_0095, imx519mipiraw_SWNR_0096, imx519mipiraw_SWNR_0097, imx519mipiraw_SWNR_0098, imx519mipiraw_SWNR_0099,
    imx519mipiraw_SWNR_0100, imx519mipiraw_SWNR_0101, imx519mipiraw_SWNR_0102, imx519mipiraw_SWNR_0103, imx519mipiraw_SWNR_0104, imx519mipiraw_SWNR_0105, imx519mipiraw_SWNR_0106, imx519mipiraw_SWNR_0107, imx519mipiraw_SWNR_0108, imx519mipiraw_SWNR_0109,
    imx519mipiraw_SWNR_0110, imx519mipiraw_SWNR_0111, imx519mipiraw_SWNR_0112, imx519mipiraw_SWNR_0113, imx519mipiraw_SWNR_0114, imx519mipiraw_SWNR_0115, imx519mipiraw_SWNR_0116, imx519mipiraw_SWNR_0117, imx519mipiraw_SWNR_0118, imx519mipiraw_SWNR_0119,
    imx519mipiraw_SWNR_0120, imx519mipiraw_SWNR_0121, imx519mipiraw_SWNR_0122, imx519mipiraw_SWNR_0123, imx519mipiraw_SWNR_0124, imx519mipiraw_SWNR_0125, imx519mipiraw_SWNR_0126, imx519mipiraw_SWNR_0127, imx519mipiraw_SWNR_0128, imx519mipiraw_SWNR_0129,
    imx519mipiraw_SWNR_0130, imx519mipiraw_SWNR_0131, imx519mipiraw_SWNR_0132, imx519mipiraw_SWNR_0133, imx519mipiraw_SWNR_0134, imx519mipiraw_SWNR_0135, imx519mipiraw_SWNR_0136, imx519mipiraw_SWNR_0137, imx519mipiraw_SWNR_0138, imx519mipiraw_SWNR_0139,
    imx519mipiraw_SWNR_0140, imx519mipiraw_SWNR_0141, imx519mipiraw_SWNR_0142, imx519mipiraw_SWNR_0143, imx519mipiraw_SWNR_0144, imx519mipiraw_SWNR_0145, imx519mipiraw_SWNR_0146, imx519mipiraw_SWNR_0147, imx519mipiraw_SWNR_0148, imx519mipiraw_SWNR_0149,
    imx519mipiraw_SWNR_0150, imx519mipiraw_SWNR_0151, imx519mipiraw_SWNR_0152, imx519mipiraw_SWNR_0153, imx519mipiraw_SWNR_0154, imx519mipiraw_SWNR_0155, imx519mipiraw_SWNR_0156, imx519mipiraw_SWNR_0157, imx519mipiraw_SWNR_0158, imx519mipiraw_SWNR_0159,
    imx519mipiraw_SWNR_0160, imx519mipiraw_SWNR_0161, imx519mipiraw_SWNR_0162, imx519mipiraw_SWNR_0163, imx519mipiraw_SWNR_0164, imx519mipiraw_SWNR_0165, imx519mipiraw_SWNR_0166, imx519mipiraw_SWNR_0167, imx519mipiraw_SWNR_0168, imx519mipiraw_SWNR_0169,
    imx519mipiraw_SWNR_0170, imx519mipiraw_SWNR_0171, imx519mipiraw_SWNR_0172, imx519mipiraw_SWNR_0173, imx519mipiraw_SWNR_0174, imx519mipiraw_SWNR_0175, imx519mipiraw_SWNR_0176, imx519mipiraw_SWNR_0177, imx519mipiraw_SWNR_0178, imx519mipiraw_SWNR_0179,
    imx519mipiraw_SWNR_0180, imx519mipiraw_SWNR_0181, imx519mipiraw_SWNR_0182, imx519mipiraw_SWNR_0183, imx519mipiraw_SWNR_0184, imx519mipiraw_SWNR_0185, imx519mipiraw_SWNR_0186, imx519mipiraw_SWNR_0187, imx519mipiraw_SWNR_0188, imx519mipiraw_SWNR_0189,
    imx519mipiraw_SWNR_0190, imx519mipiraw_SWNR_0191, imx519mipiraw_SWNR_0192, imx519mipiraw_SWNR_0193, imx519mipiraw_SWNR_0194, imx519mipiraw_SWNR_0195, imx519mipiraw_SWNR_0196, imx519mipiraw_SWNR_0197, imx519mipiraw_SWNR_0198, imx519mipiraw_SWNR_0199,
    imx519mipiraw_SWNR_0200, imx519mipiraw_SWNR_0201, imx519mipiraw_SWNR_0202, imx519mipiraw_SWNR_0203, imx519mipiraw_SWNR_0204, imx519mipiraw_SWNR_0205, imx519mipiraw_SWNR_0206, imx519mipiraw_SWNR_0207, imx519mipiraw_SWNR_0208, imx519mipiraw_SWNR_0209,
    imx519mipiraw_SWNR_0210, imx519mipiraw_SWNR_0211, imx519mipiraw_SWNR_0212, imx519mipiraw_SWNR_0213, imx519mipiraw_SWNR_0214, imx519mipiraw_SWNR_0215, imx519mipiraw_SWNR_0216, imx519mipiraw_SWNR_0217, imx519mipiraw_SWNR_0218, imx519mipiraw_SWNR_0219,
    imx519mipiraw_SWNR_0220, imx519mipiraw_SWNR_0221, imx519mipiraw_SWNR_0222, imx519mipiraw_SWNR_0223, imx519mipiraw_SWNR_0224, imx519mipiraw_SWNR_0225, imx519mipiraw_SWNR_0226, imx519mipiraw_SWNR_0227, imx519mipiraw_SWNR_0228, imx519mipiraw_SWNR_0229,
    imx519mipiraw_SWNR_0230, imx519mipiraw_SWNR_0231, imx519mipiraw_SWNR_0232, imx519mipiraw_SWNR_0233, imx519mipiraw_SWNR_0234, imx519mipiraw_SWNR_0235, imx519mipiraw_SWNR_0236, imx519mipiraw_SWNR_0237, imx519mipiraw_SWNR_0238, imx519mipiraw_SWNR_0239,
    imx519mipiraw_SWNR_0240, imx519mipiraw_SWNR_0241, imx519mipiraw_SWNR_0242, imx519mipiraw_SWNR_0243, imx519mipiraw_SWNR_0244, imx519mipiraw_SWNR_0245, imx519mipiraw_SWNR_0246, imx519mipiraw_SWNR_0247, imx519mipiraw_SWNR_0248, imx519mipiraw_SWNR_0249,
    imx519mipiraw_SWNR_0250, imx519mipiraw_SWNR_0251, imx519mipiraw_SWNR_0252, imx519mipiraw_SWNR_0253, imx519mipiraw_SWNR_0254, imx519mipiraw_SWNR_0255, imx519mipiraw_SWNR_0256, imx519mipiraw_SWNR_0257, imx519mipiraw_SWNR_0258, imx519mipiraw_SWNR_0259,
    imx519mipiraw_SWNR_0260, imx519mipiraw_SWNR_0261, imx519mipiraw_SWNR_0262, imx519mipiraw_SWNR_0263, imx519mipiraw_SWNR_0264, imx519mipiraw_SWNR_0265, imx519mipiraw_SWNR_0266, imx519mipiraw_SWNR_0267, imx519mipiraw_SWNR_0268, imx519mipiraw_SWNR_0269,
    imx519mipiraw_SWNR_0270, imx519mipiraw_SWNR_0271, imx519mipiraw_SWNR_0272, imx519mipiraw_SWNR_0273, imx519mipiraw_SWNR_0274, imx519mipiraw_SWNR_0275, imx519mipiraw_SWNR_0276, imx519mipiraw_SWNR_0277, imx519mipiraw_SWNR_0278, imx519mipiraw_SWNR_0279,
    imx519mipiraw_SWNR_0280, imx519mipiraw_SWNR_0281, imx519mipiraw_SWNR_0282, imx519mipiraw_SWNR_0283, imx519mipiraw_SWNR_0284, imx519mipiraw_SWNR_0285, imx519mipiraw_SWNR_0286, imx519mipiraw_SWNR_0287, imx519mipiraw_SWNR_0288, imx519mipiraw_SWNR_0289,
    imx519mipiraw_SWNR_0290, imx519mipiraw_SWNR_0291, imx519mipiraw_SWNR_0292, imx519mipiraw_SWNR_0293, imx519mipiraw_SWNR_0294, imx519mipiraw_SWNR_0295, imx519mipiraw_SWNR_0296, imx519mipiraw_SWNR_0297, imx519mipiraw_SWNR_0298, imx519mipiraw_SWNR_0299,
    imx519mipiraw_SWNR_0300, imx519mipiraw_SWNR_0301, imx519mipiraw_SWNR_0302, imx519mipiraw_SWNR_0303, imx519mipiraw_SWNR_0304, imx519mipiraw_SWNR_0305, imx519mipiraw_SWNR_0306, imx519mipiraw_SWNR_0307, imx519mipiraw_SWNR_0308, imx519mipiraw_SWNR_0309,
    imx519mipiraw_SWNR_0310, imx519mipiraw_SWNR_0311, imx519mipiraw_SWNR_0312, imx519mipiraw_SWNR_0313, imx519mipiraw_SWNR_0314, imx519mipiraw_SWNR_0315, imx519mipiraw_SWNR_0316, imx519mipiraw_SWNR_0317, imx519mipiraw_SWNR_0318, imx519mipiraw_SWNR_0319,
    imx519mipiraw_SWNR_0320, imx519mipiraw_SWNR_0321, imx519mipiraw_SWNR_0322, imx519mipiraw_SWNR_0323, imx519mipiraw_SWNR_0324, imx519mipiraw_SWNR_0325, imx519mipiraw_SWNR_0326, imx519mipiraw_SWNR_0327, imx519mipiraw_SWNR_0328, imx519mipiraw_SWNR_0329,
    imx519mipiraw_SWNR_0330, imx519mipiraw_SWNR_0331, imx519mipiraw_SWNR_0332, imx519mipiraw_SWNR_0333, imx519mipiraw_SWNR_0334, imx519mipiraw_SWNR_0335, imx519mipiraw_SWNR_0336, imx519mipiraw_SWNR_0337, imx519mipiraw_SWNR_0338, imx519mipiraw_SWNR_0339,
    imx519mipiraw_SWNR_0340, imx519mipiraw_SWNR_0341, imx519mipiraw_SWNR_0342, imx519mipiraw_SWNR_0343, imx519mipiraw_SWNR_0344, imx519mipiraw_SWNR_0345, imx519mipiraw_SWNR_0346, imx519mipiraw_SWNR_0347, imx519mipiraw_SWNR_0348, imx519mipiraw_SWNR_0349,
    imx519mipiraw_SWNR_0350, imx519mipiraw_SWNR_0351, imx519mipiraw_SWNR_0352, imx519mipiraw_SWNR_0353, imx519mipiraw_SWNR_0354, imx519mipiraw_SWNR_0355, imx519mipiraw_SWNR_0356, imx519mipiraw_SWNR_0357, imx519mipiraw_SWNR_0358, imx519mipiraw_SWNR_0359,
    imx519mipiraw_SWNR_0360, imx519mipiraw_SWNR_0361, imx519mipiraw_SWNR_0362, imx519mipiraw_SWNR_0363, imx519mipiraw_SWNR_0364, imx519mipiraw_SWNR_0365, imx519mipiraw_SWNR_0366, imx519mipiraw_SWNR_0367, imx519mipiraw_SWNR_0368, imx519mipiraw_SWNR_0369,
    imx519mipiraw_SWNR_0370, imx519mipiraw_SWNR_0371, imx519mipiraw_SWNR_0372, imx519mipiraw_SWNR_0373, imx519mipiraw_SWNR_0374, imx519mipiraw_SWNR_0375, imx519mipiraw_SWNR_0376, imx519mipiraw_SWNR_0377, imx519mipiraw_SWNR_0378, imx519mipiraw_SWNR_0379,
    imx519mipiraw_SWNR_0380, imx519mipiraw_SWNR_0381, imx519mipiraw_SWNR_0382, imx519mipiraw_SWNR_0383, imx519mipiraw_SWNR_0384, imx519mipiraw_SWNR_0385, imx519mipiraw_SWNR_0386, imx519mipiraw_SWNR_0387, imx519mipiraw_SWNR_0388, imx519mipiraw_SWNR_0389,
    imx519mipiraw_SWNR_0390, imx519mipiraw_SWNR_0391, imx519mipiraw_SWNR_0392, imx519mipiraw_SWNR_0393, imx519mipiraw_SWNR_0394, imx519mipiraw_SWNR_0395, imx519mipiraw_SWNR_0396, imx519mipiraw_SWNR_0397, imx519mipiraw_SWNR_0398, imx519mipiraw_SWNR_0399,
},
.CA_LTM = {
    imx519mipiraw_CA_LTM_0000, imx519mipiraw_CA_LTM_0001, imx519mipiraw_CA_LTM_0002, imx519mipiraw_CA_LTM_0003, imx519mipiraw_CA_LTM_0004, imx519mipiraw_CA_LTM_0005, imx519mipiraw_CA_LTM_0006, imx519mipiraw_CA_LTM_0007, imx519mipiraw_CA_LTM_0008, imx519mipiraw_CA_LTM_0009,
    imx519mipiraw_CA_LTM_0010, imx519mipiraw_CA_LTM_0011, imx519mipiraw_CA_LTM_0012, imx519mipiraw_CA_LTM_0013, imx519mipiraw_CA_LTM_0014, imx519mipiraw_CA_LTM_0015, imx519mipiraw_CA_LTM_0016, imx519mipiraw_CA_LTM_0017, imx519mipiraw_CA_LTM_0018, imx519mipiraw_CA_LTM_0019,
    imx519mipiraw_CA_LTM_0020, imx519mipiraw_CA_LTM_0021, imx519mipiraw_CA_LTM_0022, imx519mipiraw_CA_LTM_0023, imx519mipiraw_CA_LTM_0024, imx519mipiraw_CA_LTM_0025, imx519mipiraw_CA_LTM_0026, imx519mipiraw_CA_LTM_0027, imx519mipiraw_CA_LTM_0028, imx519mipiraw_CA_LTM_0029,
},
.ClearZoom = {
    imx519mipiraw_ClearZoom_0000, imx519mipiraw_ClearZoom_0001, imx519mipiraw_ClearZoom_0002, imx519mipiraw_ClearZoom_0003, imx519mipiraw_ClearZoom_0004, imx519mipiraw_ClearZoom_0005, imx519mipiraw_ClearZoom_0006,
},
.SWNR_THRES = {
    imx519mipiraw_SWNR_THRES_0000, imx519mipiraw_SWNR_THRES_0001, imx519mipiraw_SWNR_THRES_0002, imx519mipiraw_SWNR_THRES_0003, imx519mipiraw_SWNR_THRES_0004, imx519mipiraw_SWNR_THRES_0005, imx519mipiraw_SWNR_THRES_0006, imx519mipiraw_SWNR_THRES_0007, imx519mipiraw_SWNR_THRES_0008, imx519mipiraw_SWNR_THRES_0009,
    imx519mipiraw_SWNR_THRES_0010, imx519mipiraw_SWNR_THRES_0011, imx519mipiraw_SWNR_THRES_0012, imx519mipiraw_SWNR_THRES_0013, imx519mipiraw_SWNR_THRES_0014, imx519mipiraw_SWNR_THRES_0015, imx519mipiraw_SWNR_THRES_0016, imx519mipiraw_SWNR_THRES_0017, imx519mipiraw_SWNR_THRES_0018, imx519mipiraw_SWNR_THRES_0019,
},

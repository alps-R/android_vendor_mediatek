/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */


STATIC_METADATA2_BEGIN(PLATFORM, AVAILABLE_KEYS, SENSOR_DRVNAME_IMX258_MIPI_RAW_bayermono)
//------------------------------------------------------------------------------
//  android.request
//------------------------------------------------------------------------------
    //==========================================================================
    APPEND_METADATA_BEGIN(MTK_REQUEST_AVAILABLE_REQUEST_KEYS)//new hidden
        //APPEND_ENTRY_VALUE(MTK_CONTROL_AF_REGIONS, MINT32)
        // face feature
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_GESTURE_MODE, MINT32)
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_SMILE_DETECT_MODE, MINT32)
        // multi cam
        APPEND_ENTRY_VALUE(MTK_MULTI_CAM_FEATURE_MODE, MINT32)
    APPEND_METADATA_END()
    //==========================================================================
    APPEND_METADATA_BEGIN(MTK_REQUEST_AVAILABLE_RESULT_KEYS)//new hidden
        //APPEND_ENTRY_VALUE(MTK_CONTROL_AF_REGIONS, MINT32)
        // face feature
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_GESTURE_MODE, MINT32)
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_GESTURE_RESULT, MINT32)
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_SMILE_DETECT_MODE, MINT32)
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_SMILE_DETECT_RESULT, MINT32)
    APPEND_METADATA_END()
    //==========================================================================
    APPEND_METADATA_BEGIN(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS)//new hidden
        APPEND_ENTRY_VALUE(MTK_SENSOR_OPAQUE_RAW_SIZE, MINT32)
        APPEND_ENTRY_VALUE(MTK_SENSOR_ORIENTATION, MINT32)
        APPEND_ENTRY_VALUE(MTK_SENSOR_INFO_FACING, MINT32)
        APPEND_ENTRY_VALUE(MTK_STATISTICS_INFO_MAX_FACE_COUNT, MINT32)
        // face feature
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_AVAILABLE_GESTURE_MODES, MINT32)
        APPEND_ENTRY_VALUE(MTK_FACE_FEATURE_AVAILABLE_SMILE_DETECT_MODES, MINT32)
        // reprocessing
        APPEND_ENTRY_VALUE(MTK_REPROCESS_MAX_CAPTURE_STALL, MINT32)
    APPEND_METADATA_END()
    //==========================================================================
    APPEND_METADATA_BEGIN(MTK_REQUEST_AVAILABLE_SESSION_KEYS)
    APPEND_METADATA_END()
    //==========================================================================


//------------------------------------------------------------------------------
STATIC_METADATA_END()


[single_bin]
logo.bin=logo
boot.img=boot
recovery.img=recovery
md1arm7.img=md1arm7
md1dsp.img=md1dsp
md3rom.img=md3rom
spmfw.img=spmfw
mcupmfw.img=mcupmfw
dtbo.img=dtbo
odmdtbo.img=dtbo
mnb.img=mnb

[multi_bin]
tee.img=atf,atf_dram,tee
lk.img=lk,lk_main_dtb
md1img.img=md1rom,md1dsp,md1drdi
loader_ext.img=loader_ext_dram,loader_ext_etc
boot_para.img=dconfig,dconfig-dt
